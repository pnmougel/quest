import {FieldDefinition} from './FieldDefinition';
import {ModelRegistry} from './ModelRegistry';
import {StringUtils} from '../utils/StringUtils';

export interface ModelDefinitionOptions {
    name?: string;
    description?: string;
    isTable?: boolean
}

export function Model(options: ModelDefinitionOptions = {}) {
    return function(constructor: Function) {
        const modelDefinition = ModelRegistry.Instance.addOrGetModel(constructor.name);
        modelDefinition.name = constructor.name;
        modelDefinition.description = options.description;

        if(typeof options.name !== 'undefined') {
            modelDefinition.tableName = options.name;
        } else {
            modelDefinition.tableName = StringUtils.snakize(constructor.name);
        }

        if(typeof options.isTable !== 'undefined') {
            modelDefinition.isTable = options.isTable;
        }

        ModelRegistry.Instance.registerTableForModel(modelDefinition);
    }
}

export class ModelDefinition {
    tableName: string = '';
    name: string = '';
    description?: string = '';

    // List of operations for the Model
    operations: string[] = ['sql'];
    isTable: boolean = true;

    constructor() {}

    get dependencies(): string[] {
        return this.fields.filter(f => !!f.fkTable).map(f => '' + f.fkTable)
    }

    private _fields = new Map<string, FieldDefinition>();
    get fields() {
        return Array.from(this._fields.values());
    }

    addField(field: FieldDefinition) {
        this._fields.set(field.name, field);
        field.notIn.forEach(operation => {
            this.operations.push(operation);
        })
    }

    get sqlCreateTableStmt() {
        if(!this.isTable) {
            return '';
        }
        const fieldsDefinition = this.fields
            .filter(f => f.notIn.indexOf('sql') === -1)
            .map(f => f.sqlCreateFieldStmt);
        this.fields.forEach(f => {
            if (!!f.fkDefinition) {
                fieldsDefinition.push(f.fkDefinition);
            }
        });
        fieldsDefinition.push('primary key (id)');
        return `create table if not exists ${this.tableName} (\n    ${fieldsDefinition.join(',\n    ')}\n);`
    }

    get sqlDropTableStmt() {
        if(!this.isTable) {
            return '';
        }
        return `drop table if exists ${this.tableName}`
    }
    
    swaggerSchema(operation: string): object {
        const properties: any = {};
        const fieldsForOperation = this.fields.filter(f => f.notIn.indexOf(operation) === -1);
        fieldsForOperation.forEach(f => properties[f.jsonFieldName] = f.swaggerField);
        return {
            type: 'object',
            description: this.description,
            properties,
            required: fieldsForOperation.filter(f => f.required).map(f => f.jsonFieldName)
        };
    }
}
