import {ModelRegistry} from './ModelRegistry';
import {RequestHandler} from 'express';
import {ValidationHandlerSource} from './FieldDefinition';

export function validateModel<T>(model: { new(): T ;}, operationName: string = '', source: ValidationHandlerSource): RequestHandler[] {
    const modelRegistry = ModelRegistry.Instance;
    if(!modelRegistry.isModelRegistered(model.name)) {
        // TODO: Throw error
    }
    const modelDefinition = modelRegistry.addOrGetModel(model.name);

    const fields = modelDefinition.fields.filter(f => {
        if(!operationName) {
            return true;
        } else {
            return f.notIn.indexOf(operationName) === -1;
        }
    });
    return fields.map(field => field.validationHandlers(source))
}
