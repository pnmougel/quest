import {StringUtils} from '../utils/StringUtils';
import {plainToClass} from 'class-transformer';

export function toModel<T>(model: { new(): T ;}, value: any) : T {
    const plain: any = {};
    Object.keys(value).forEach(key => {
        plain[StringUtils.camelize(key)] = value[key];
    });
    return plainToClass(model, plain) as T;
}

export function toArrayModel<T>(model: { new(): T ;}, value: any[]) : Array<T> {
    return value.map(v => toModel(model, v));
}
