import {ModelDefinition} from './ModelDefinition';

export class ModelRegistry {
    private static _instance: ModelRegistry;

    private _models = new Map<string, ModelDefinition>();
    private _tableNameToModel = new Map<string, ModelDefinition>();
    
    get models(): ModelDefinition[] {
        return Array.from(this._models.values())
    }

    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

    isModelRegistered(name: string) {
        return this._models.has(name);
    }

    addOrGetModel(className: string): ModelDefinition {
        if(!this._models.has(className)) {
            this._models.set(className, new ModelDefinition());
        }
        return this._models.get(className) as ModelDefinition;
    }
    
    getModelForTable(tableName: string): ModelDefinition {
        return this._tableNameToModel.get(tableName) as ModelDefinition
    }

    registerTableForModel(model: ModelDefinition) {
        this._tableNameToModel.set(model.tableName, model);
    }

    // Do nothing but force loading annotations
    registerModel(models: any[]) {

    }
}
