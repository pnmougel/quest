import {body, CustomValidator} from 'express-validator/check';
import {ModelRegistry} from './ModelRegistry';
import {StringUtils} from '../utils/StringUtils';
import 'reflect-metadata';
import {NextFunction, Request, RequestHandler, Response} from 'express';
import {idExists, isArray, isIn} from '../validators/validator';

export enum ValidationHandlerSource {
    Body = 'body',
    Query = 'query',
    Path = 'path',
    Headers = 'header',
}

export interface FieldDefinitionOptions {
    name?: string;
    description?: string;
    required?: boolean;
    type?: string;
    defaultSql?: string;
    defaultValue?: any;
    fkTable?: string | null;
    enum?: object;
    validators?: CustomValidator[];
    swaggerFormat?: string;

    arrayItemsType?: object;

    // List of operations where the field is discarded
    // To discard the field from table, use "sql"
    notIn?: string[]
}


class FieldValidation {
    constructor(private readonly request: Request,
                public readonly field: FieldDefinition,
                public readonly value: any,
                public readonly location: ValidationHandlerSource
                ) {
    }

    addError(message: any) {
        if(!this.request._validationErrors) {
            this.request._validationErrors = [];
        }
        this.request._validationErrors.push({
            location: this.location,
            param: this.field.jsonFieldName,
            value: this.value,
            msg: message
        })
    }

}

export function Field(fieldDefinitionOptions: FieldDefinitionOptions = {}) {
    return function(target: any, name: string) {
        const fieldDefinition = new FieldDefinition(target, name, fieldDefinitionOptions);
        ModelRegistry.Instance.addOrGetModel(target.constructor.name).addField(fieldDefinition);
    }
}

export class FieldDefinition {
    jsonFieldName: string = '';
    sqlFieldName: string = '';

    notIn: string[] = [];

    baseType: string = 'String';
    sqlType: string = 'text';
    swaggerType: string = '';
    swaggerFormat: string = '';

    defaultSql?: string;
    defaultValue?: any;

    required: boolean = true;
    fkTable?: string;
    description?: string;

    isEnum: boolean = false;
    enumValues?: string[];

    customValidators: CustomValidator[] = [];

    constructor(private readonly target: any, public readonly name: string, private options: FieldDefinitionOptions = {}) {
        const designType = Reflect.getMetadata('design:type', target, name);
        const fieldType = designType.name;
        
        // Set operations where the field should be discarded
        if(typeof options.notIn !== 'undefined') {
            this.notIn = options.notIn;
        }
        
        // Set field name
        if(!!options.name) {
            this.jsonFieldName = StringUtils.snakize(this.name);
            this.sqlFieldName = this.name;
        } else {
            this.jsonFieldName = StringUtils.snakize(this.name);
            this.sqlFieldName = StringUtils.snakize(this.name);
        }

        if(typeof options.required !== 'undefined') {
            this.required = options.required;
        }
        if(typeof options.defaultValue !== 'undefined') {
            this.required = false;
        }

        this.description = options.description;

        // Handle enum
        if(!!options.enum) {
            this.isEnum = true;
            this.enumValues = Object.values(options.enum).filter(k => typeof k !== "number");
            this.customValidators.push(isIn(this.enumValues));
        }

        if(!!options.type) {
            this.sqlType = options.type;
        } else {
            this.baseType = fieldType;
            if(this.isEnum) {
                this.sqlType = 'text';
                this.swaggerType = 'text';
            } else if(fieldType === 'Number') {
                this.swaggerType = 'number';
                this.sqlType = 'integer';
            } else if(fieldType === 'Boolean') {
                this.swaggerType = 'boolean';
                this.sqlType = 'boolean';
            } else if(fieldType === 'Date') {
                this.sqlType = 'datetime';
                this.swaggerType = 'text';
            } else if(fieldType === 'String') {
                this.sqlType = 'text';
                this.swaggerType = 'text';
            } else if(fieldType === 'Array') {
                this.swaggerType = 'array';
                this.sqlType = 'text';
                this.sqlFieldName = this.sqlFieldName + '_json';
                this.customValidators.push(isArray())
            } else if(fieldType === 'object') {
                this.swaggerType = 'object';
                this.sqlType = 'text';
                this.sqlFieldName = this.sqlFieldName + '_json';
            } else {
                // Default field type handling
                console.log('Unable to map field type ' + fieldType);
                this.swaggerType = 'text';
                this.sqlType = 'text';
                this.sqlFieldName = this.sqlFieldName + '_json';
            }
        }

        // Build swagger format field
        if(!!options.swaggerFormat) {
            this.swaggerFormat = options.swaggerFormat;
        } else if(this.sqlType === 'datetime') {
            this.swaggerFormat = 'date-time';
        }

        // Handle default values
        if(!!options.defaultSql) {
            this.defaultSql = options.defaultSql;
        } else if(this.sqlFieldName === 'created_at') {
            this.defaultSql = "(datetime('now','utc'))";
        }
        this.defaultValue = options.defaultValue;

        // Set fk table
        if(typeof options.fkTable !== 'undefined') {
            if(!!options.fkTable) {
                this.fkTable = options.fkTable;
            }
        } else if(this.sqlFieldName.endsWith('_id')) {
            this.fkTable = this.sqlFieldName.slice(0, -3);
        }
        if(typeof this.fkTable !== 'undefined') {
            this.customValidators.push(idExists(this.fkTable));
        }

        // Validators
        if(typeof options.validators !== 'undefined') {
            this.customValidators = options.validators;
        }
    }

    validationHandlers(source: ValidationHandlerSource = ValidationHandlerSource.Body): RequestHandler {
        return (req: Request, res: Response, next: NextFunction) => {
            let value: any = null;
            if(source === ValidationHandlerSource.Body) {
                value = req.body[this.jsonFieldName];
            } else if(source === ValidationHandlerSource.Query) {
                value = req.query[this.jsonFieldName];
            }
            const fieldValidation = new FieldValidation(req, this, value, source);
            try {
                const isDefined = typeof value !== 'undefined';
                if(this.required && !isDefined) {
                    fieldValidation.addError(`Value required`)
                }
                if(isDefined) {
                    // Perform custom validation
                    this.customValidators.forEach(validator => {
                        try {
                            const res = validator(value, { req, location: req.baseUrl, path: req.path });
                            if(!!res) {
                                fieldValidation.addError(res)
                            }
                        } catch (e) {
                            fieldValidation.addError(e)
                        }
                    });
                } else if(typeof this.defaultValue !== 'undefined') {
                    // Set default value
                    if(source === ValidationHandlerSource.Body) {
                        req.body[this.jsonFieldName] = this.defaultValue;
                    } else if(source === ValidationHandlerSource.Query) {
                        req.query[this.jsonFieldName] = this.defaultValue;
                    }
                }
            } catch (e) {
                fieldValidation.addError({
                    description: `Unexpected error during validation`,
                    error: e
                });
            }
            next();
        };
    }

    get sqlCreateFieldStmt(): string {
        const defaultStmt = !!this.defaultSql ? ` default ${this.defaultSql}` : '';
        const nullableStmt = this.required ? ' not null' : '';
        return `${this.sqlFieldName} ${this.sqlType}${defaultStmt}${nullableStmt}`
    }

    get fkDefinition() {
        if(!!this.fkTable) {
            return `foreign key(${this.sqlFieldName}) references ${this.fkTable}(id) on delete cascade`
        }
    }

    get swaggerField() {
        const def: any = {
            type: this.swaggerType
        };
        if(this.isEnum) {
            def.enum = this.enumValues;
        }
        if(this.defaultValue !== null) {
            def.default = this.defaultValue;
        }
        if(!!this.description) {
            def.description = this.description;
        }
        if(typeof this.options.arrayItemsType !== 'undefined') {
            def.items = this.options.arrayItemsType;
        }
        return def;
    }

    get swaggerParam() {
        const schema: any = {
            type: this.swaggerType,
            default: this.defaultValue
        };
        if(this.defaultValue !== null) {
            schema.default = this.defaultValue;
        }
        if(this.isEnum) {
            schema.enum = this.enumValues;
        }
        if(typeof this.options.arrayItemsType !== 'undefined') {
            schema.items = this.options.arrayItemsType;
        }
        return {
            in: 'query',
            name: this.name,
            type: this.swaggerType,
            required: this.required,
            description: this.description,
            schema
        };
    }
}
