import Database from 'better-sqlite3';
import {plainToClass} from 'class-transformer';
import {ClassType} from 'class-transformer/ClassTransformer';

export class Db {
    static connection: Database.Database = new Database('db/data.db', {verbose: console.log});

    static exec(source: string) {
        return Db.connection.exec(source)
    }

    static prepare<T>(source: string) {
        return Db.connection.prepare<T>(source)
    }

    static close() {
        return Db.connection.close()
    }

    static checkpoint(databaseName?: string) {
        return Db.connection.checkpoint(databaseName)
    }

    static insert(table: string, values: object) {
        const params = Db.wrapSqlParams(values);
        const fieldNames = Object.keys(params);
        const paramNames = fieldNames.map(e => '$' + e).join(',');
        const query = `insert into ${table} (${fieldNames.join(',')}) values (${paramNames})`;
        const stmt = Db.prepare(query).run(params);
        return stmt;
    }

    static count(table: string, filters: object) {
        const whereStmt = Db.buildWhereStmt(filters, 'AND');
        const query = `select count(1) as count from ${table} where ${whereStmt.sql}`;
        const stmt = Db.prepare(query).get(Db.wrapSqlParams(whereStmt.params));
        return stmt.count;
    }

    static idExists(table: string, id: any) {
        const query = `select count(1) as count from ${table} where ROWID = ${id}`;
        const stmt = Db.prepare(query).get({id});
        return stmt.count === 1;
    }

    static select(table: string, filters: object = {}, size: number = 20, from: number = 0) {
        const whereStmt = Db.buildWhereStmt(filters, 'AND');
        const query = `select * from ${table}${whereStmt.sql}`;
        const items = Db.prepare(query).all(Db.wrapSqlParams(whereStmt.params));
        return items.map(item => Db.unwrap(item));
    }

    static selectAs<T>(cls: ClassType<T>, table: string, filters: object = {}, size: number = 20, from: number = 0): T[] {
        return Db.select(table, filters, size, from)
            .map(item => plainToClass(cls, item))
    }

    private static buildWhereStmt(whereParams: any, joinOperator: string = 'AND') {
        const fieldNames = Object.keys(whereParams);
        if(fieldNames.length === 0) {
            return {
                sql: '',
                params: {}
            }
        }
        const params: any = {};
        const filters: string[] = [];

        fieldNames.forEach(fieldName => {
            const v = whereParams[fieldName];
            if(Array.isArray(v)) {
                filters.push(`${fieldName} IN ($${fieldName})`);
                params[fieldName] = v.join(',');
            } else if(typeof v === 'object' && !!v && !!v.operator && !!v.value) {
                filters.push(`${fieldName} ${v.operator} $${fieldName}`);
                params[fieldName] = v.value;
            } else if(!!v) {
                filters.push(`${fieldName} = $${fieldName}`);
                params[fieldName] = v;
            }
        });
        return {
            sql: ' where (' + filters.join(`) ${joinOperator} (`) + ')',
            params
        }
    }

    static update(table: string, id: number, values: object) {
        const params = Db.wrapSqlParams(values);
        const fieldNames = Object.keys(params).map(fieldName => {
            return `${fieldName} = $${fieldName}`
        }).join(',');
        const query = `update ${table} set ${fieldNames} where ROWID = ${id}`;
        const stmt = Db.prepare(query).run(params);
        return stmt;
    }

    static delete(table: string, id: number) {
        return Db.prepare(`delete from ${table} where ROWID = $id`).run({id});
    }

    static get(table: string, id: number) {
        const item = Db.prepare(`select * from ${table} where ROWID = $id`).get({id});
        return Db.unwrap(item);
    }

    static getAs<T>(cls: ClassType<T>, table: string, id: number): T {
        const item = Db.prepare(`select * from ${table} where ROWID = $id`).get({id});
        const unwrapped = Db.unwrap(item);
        return plainToClass(cls, unwrapped);
    }

    static wrapSqlParams(e: any): any {
        let wrapped: any = {};
        Object.keys(e).forEach(k => {
            if (e[k] instanceof Array || e[k] instanceof Object) {
                wrapped[k + '_json'] = JSON.stringify(e[k]);
            } else {
                wrapped[k] = e[k];
            }
        });
        return wrapped;
    }

    static unwrap<T>(e: any) {
        if(!e) {
            return null;
        }
        let unwrapped: any = {};
        Object.keys(e).forEach(k => {
            if(k.endsWith('_json')) {
                unwrapped[k.slice(0, -5)] = JSON.parse(e[k]);
            } else {
                unwrapped[k] = e[k];
            }
        });
        return unwrapped as T;
    }
}
