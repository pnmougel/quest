import {Request} from 'express';
import {Db} from '../database/Db';
import {CustomValidator} from 'express-validator/check';

export interface RequestValidationParams {
    req: Request;
    location: string;
    path: string;
}

export const EnvParamValidator = (value: string, requestCtx: RequestValidationParams) => {
    const environment = Db.get('environments', parseInt(value));
    if(!!environment) {
        requestCtx.req.esEnvironment = environment;
        return true;
    } else {
        throw new Error(`Missing environment with id ${value}`);
    }
};


export function isInt(minValue: number = Number.MIN_SAFE_INTEGER, maxValue: number = Number.MAX_SAFE_INTEGER): CustomValidator {
    return (value: any, requestCtx: RequestValidationParams) => {
        const intValue = parseInt(value);
        if(isNaN(intValue)) {
            return "Not a number"
        }
        if(intValue < minValue) {
            return `Out of bounds [${minValue}, ${maxValue}]`
        } else if(intValue > minValue) {
            return `Out of bounds [${minValue}, ${maxValue}]`
        }
        return false;
    }
}

export const positiveInt = isInt(0, Number.MAX_SAFE_INTEGER);

export const notEmpty: CustomValidator = (value: string, requestCtx: RequestValidationParams) => {
    if (!value || value.trim().length === 0) {
        return 'Empty value';
    }
    return false;
};

export function isIn(enumValues: any[]): CustomValidator {
    return (value: any, requestCtx: RequestValidationParams) => {
        if (!!enumValues && enumValues.indexOf(value) === -1) {
            return `Must be one of ${enumValues.join(', ')}`;
        }
    };
}

export function idExists(table: string): CustomValidator {
    return (value: any, requestCtx: RequestValidationParams) => {
        if(!Db.idExists(table, value)) {
            return `Missing id ${value} in table ${table}`;
        }
    };
}

export function isArray(): CustomValidator {
    return (value: any, requestCtx: RequestValidationParams) => {
        if(!Array.isArray(value)) {
            return `Not an array`;
        }
    };
}
