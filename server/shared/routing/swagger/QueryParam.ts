import {ModelRegistry} from '../../model/ModelRegistry';
import {RequestHandler} from 'express';
import {validateModel} from '../../model/validators';
import {ValidationHandlerSource} from '../../model/FieldDefinition';
import {SwaggerParam} from './SwaggerParam';

export function queryParam(cls: any) {
    return new QueryParam(cls);
}

export class QueryParam implements SwaggerParam {
    params: any[] = [];

    constructor(private cls: any) {
        const modelDefinition = ModelRegistry.Instance.addOrGetModel(cls.name);
        this.params = modelDefinition.fields.map(field => field.swaggerParam );
    }

    getSwaggerDef(): object[] {
        return this.params;
    }

    getValidators(): RequestHandler[] {
        return validateModel(this.cls, '', ValidationHandlerSource.Query);
    }
}
