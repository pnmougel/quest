import {RequestHandler} from 'express';

export interface SwaggerParam {
    getSwaggerDef(): object[];
    getValidators(): RequestHandler[];
}
