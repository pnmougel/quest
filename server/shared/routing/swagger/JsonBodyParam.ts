import {ModelRegistry} from '../../model/ModelRegistry';
import {SwaggerParam} from './SwaggerParam';
import {validateModel} from '../../model/validators';
import {RequestHandler} from 'express';
import {ValidationHandlerSource} from '../../model/FieldDefinition';

export function bodyParam(cls: any, operation?: string) {
    return new JsonBodyParam(cls, operation)
}

export class JsonBodyParam implements SwaggerParam {
    description?: string;
    name: string = '';
    schema: any = {};

    constructor(private cls: any, private operation?: string) {
        const modelDefinition = ModelRegistry.Instance.addOrGetModel(cls.name);
        this.name = modelDefinition.name;
        if(typeof modelDefinition.description !== 'undefined') {
            this.description = modelDefinition.description;
        }
        const suffix = typeof operation === 'undefined' ? '' : '_' + operation;
        this.schema = {
            $ref: `#/definitions/${this.name}${suffix}`
        }
    }

    getSwaggerDef() {
        return [{
            in: 'body',
            name: this.name,
            description: this.description,
            required: true,
            schema: this.schema
        }]
    }

    getValidators(): RequestHandler[] {
        const op = typeof this.operation === 'undefined' ? '' : this.operation;
        return validateModel(this.cls, op, ValidationHandlerSource.Body);
    }
}
