import {SwaggerParam} from './SwaggerParam';
import {RequestHandler} from 'express';

export function pathParam(name: string, description: string, schema: object | string) {
    return new PathParam(name, description, schema)
}

export class PathParam implements SwaggerParam {
    schema: any;

    constructor(public readonly name: string,
                public readonly description: string,
                schema: object | string) {
        if(typeof schema === 'string') {
            this.schema = {
                type: schema
            }
        } else {
            this.schema = schema;
        }
    }


    getSwaggerDef() {
        return [{
            in: 'path',
            name: this.name,
            description: this.description,
            required: true,
            schema: this.schema
        }];
    }

    getValidators(): RequestHandler[] {
        // TODO: Add validators
        return [];
    }
}
