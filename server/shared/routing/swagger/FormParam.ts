import {SwaggerParam} from './SwaggerParam';
import {RequestHandler} from 'express';

export enum FormFieldType {
    Text = 'text',
    File = 'file'
}

export interface FormField {
    name: string;
    description?: string;
    type: FormFieldType;
    required?: boolean;
}

export function formParam(...fields: FormField[]) {
    return new FormParam(fields)
}

export class FormParam implements SwaggerParam {
    constructor(private fields: FormField[]) {}

    getSwaggerDef() {
        return this.fields.map(field => {
            return {
                in: 'formData',
                name: field.name,
                description: field.description,
                required: typeof field.required === 'undefined' ? true : field.required,
                type: field.type
            };
        })

    }

    getValidators(): RequestHandler[] {
        // TODO: Add validators
        return [];
    }
}
