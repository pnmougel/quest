import {RouteDefinition} from './RouteDefinition';
import {ControllerRegistry} from './ControllerRegistry';

export interface ControllerDefinitionOptions {
    name?: string;
    description?: string;
    basePath?: string;
}

export function Controller(options: ControllerDefinitionOptions = {}) {
    return function(constructor: Function) {
        const controller = ControllerRegistry.Instance.addOrGetController(constructor.name);
        if(typeof options.name === 'undefined') {
            options.name = constructor.name.replace('Controller', '');
        }
        if(typeof options.basePath === 'undefined') {
            options.basePath = '/' + options.name.toLowerCase();
        }
        if(!options.basePath.startsWith('/')) {
            options.basePath = '/' + options.basePath;
        }
        controller.options = options;
    }
}


export class ControllerDefinition {
    private _routes: RouteDefinition[] = [];

    private _options?: ControllerDefinitionOptions;
    get options(): ControllerDefinitionOptions {
        return this._options as ControllerDefinitionOptions;
    }
    set options(value: ControllerDefinitionOptions) {
        this._options = value;
    }
    get routes(): RouteDefinition[] {
        return this._routes;
    }

    get name(): string {
        return this.options.name as string;
    }

    addRoute(route: RouteDefinition) {
        // console.log('Register route ' + route.name);
        this._routes.push(route);
    }
}
