import {Router} from 'express';
import {ControllerDefinition} from './ControllerDefinition';
import {HttpMethod} from '../../domains/es-request/models/HttpMethod';

export class ControllerRegistry {
    getRouter() {
        // Build the routes
        const router = Router();
        Array.from(this._controllers.values()).forEach(controller => {
            controller.routes.map(route => {
                const methods = route.methods;
                const path = `${controller.options.basePath}${route.path}`;
                methods.forEach(method => {
                    switch (method) {
                        case HttpMethod.Get:
                            router.get(path, route.requestHandlers);
                            break;
                        case HttpMethod.Post:
                            router.post(path, route.requestHandlers);
                            break;
                        case HttpMethod.Put:
                            router.put(path, route.requestHandlers);
                            break;
                        case HttpMethod.Head:
                            router.head(path, route.requestHandlers);
                            break;
                        case HttpMethod.Delete:
                            router.delete(path, route.requestHandlers);
                            break;
                        default:
                            break
                    }
                });
            });
        });
        return router;
    }
    private _controllers = new Map<string, ControllerDefinition>();

    private static _instance: ControllerRegistry;

    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

    get controllers(): ControllerDefinition[] {
        return Array.from(this._controllers.values());
    }

    addOrGetController(name: string) {
        if(!this._controllers.has(name)) {
            this._controllers.set(name, new ControllerDefinition());
        }
        return this._controllers.get(name) as ControllerDefinition;
    }

    // Do nothing but force loading annotations
    registerController(controllers: any[]) {

    }
}
