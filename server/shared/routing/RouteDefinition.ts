import {NextFunction, Request, RequestHandler, Response} from 'express';
import {HttpMethod} from '../../domains/es-request/models/HttpMethod';
import {ControllerRegistry} from './ControllerRegistry';
import {ControllerDefinition} from './ControllerDefinition';
import {SwaggerParam} from './swagger/SwaggerParam';
import {JsonBodyParam} from './swagger/JsonBodyParam';
import {PathParam} from './swagger/PathParam';
import {validationResult} from 'express-validator/check';
import {QueryParam} from './swagger/QueryParam';
import {FormParam} from './swagger/FormParam';

export interface RouteDefinitionOptions {
    name?: string;
    description?: string;
    tags?: string[] | string;
    methods?: HttpMethod[] | HttpMethod;
    requestHandlers?: RequestHandler[];

    // Allows to set custom swagger parameters
    // swaggerParams?: SwaggerParam[];
    // bodyParams?: JsonBodyParam;

    params?: SwaggerParam[];

    // queryParams?: QueryParam;
    // pathParams?: PathParam[] | PathParam;

    // TODO: Add response format
    responseFormat?: Function;
}

export function Route(path: string, options: RouteDefinitionOptions = {}, ...params: SwaggerParam[]) {
    options.params = params;
    return buildDecorator(path, options);
}
export function GetRoute(path: string, options: RouteDefinitionOptions = {}, ...params: SwaggerParam[]) {
    options.params = params;
    return buildDecorator(path, options, HttpMethod.Get);
}
export function PostRoute(path: string, options: RouteDefinitionOptions = {}, ...params: SwaggerParam[]) {
    options.params = params;
    return buildDecorator(path, options, HttpMethod.Post);
}
export function PutRoute(path: string, options: RouteDefinitionOptions = {}, ...params: SwaggerParam[]) {
    options.params = params;
    return buildDecorator(path, options, HttpMethod.Put);
}
export function HeadRoute(path: string, options: RouteDefinitionOptions = {}, ...params: SwaggerParam[]) {
    options.params = params;
    return buildDecorator(path, options, HttpMethod.Head);
}
export function DeleteRoute(path: string, options: RouteDefinitionOptions = {}, ...params: SwaggerParam[]) {
    options.params = params;
    return buildDecorator(path, options, HttpMethod.Delete);
}


function buildDecorator(path: string, options: RouteDefinitionOptions = {}, method?: HttpMethod) {
    if(typeof method !== 'undefined') {
        options.methods = [method];
    }
    return function(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const route = new RouteDefinition(path, propertyKey, target, descriptor, options);
        const controller = ControllerRegistry.Instance.addOrGetController(target.constructor.name);
        controller.addRoute(route);
    }
}

export class RouteDefinition {
    public name: string = '';
    pathParamsNames: string[] = [];
    requestHandlers: RequestHandler[] = [];
    params: SwaggerParam[] = [];
    consumes: string[] = [];
    
    constructor(
        public readonly path: string,
        propertyKey: string,
        public readonly target: any,
        public readonly descriptor: PropertyDescriptor,
        public readonly options: RouteDefinitionOptions) {

        if(typeof options.name !== 'undefined') {
            this.name = options.name;
        } else {
            this.name = propertyKey;
        }

        if(!!options.requestHandlers) {
            this.requestHandlers = options.requestHandlers;
        }

        if(!!options.params) {
            this.params = options.params;
        }

        // Add validators
        this.params.forEach(param => {
            param.getValidators().forEach(validator => {
                this.requestHandlers.push(validator);
            })
        });

        // Add validation handler
        this.requestHandlers.push((req: Request, res: Response, next: NextFunction) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ errors: errors.array() });
            }
            next();
        });

        this.requestHandlers.push(descriptor.value);

        // Build path params
        const paramsRegexp = /\/:([^\/]+)/g;
        let match = paramsRegexp.exec(path);
        while (match != null) {
            this.pathParamsNames.push(match[1]);
            match = paramsRegexp.exec(path);
        }
    }

    get methods(): HttpMethod[] {
        if(Array.isArray(this.options.methods)) {
            return this.options.methods;
        } else if(typeof this.options.methods !== 'undefined') {
            return [this.options.methods];
        } else {
            return [];
        }
    }

    get tags(): string[] {
        if(Array.isArray(this.options.tags)) {
            return this.options.tags;
        } else if(typeof this.options.tags !== 'undefined') {
            return [this.options.tags];
        } else {
            return [];
        }
    }

    get swaggerPath() {
        return this.path.replace(/\/:([^\/]+)/g, '/{$1}')
    }

    swaggerSpec(controller: ControllerDefinition) {
        const tags: string[] = this.tags.slice();
        tags.push(controller.name);

        // Build parameters
        const parameters: any[] = [];
        this.params.forEach(param => {
            if(param instanceof FormParam) {
                this.consumes.push('multipart/form-data');
            } else if(param instanceof JsonBodyParam) {
                this.consumes.push('application/json');
            }
            param.getSwaggerDef().forEach(swaggerParam => {
                parameters.push(swaggerParam);
            });
        });

        return {
            summary: this.name,
            description: this.options.description,
            parameters,
            consumes: this.consumes,
            tags
        }
    }
}
