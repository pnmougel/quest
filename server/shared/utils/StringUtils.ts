export class StringUtils {
    static camelize(s: string) {
        return s.replace(/([-_][a-z])/ig, ($1) => $1.toUpperCase()
                .replace('_', '')
        );
    };

    static snakize(s: string) {
        return s.replace(/[\w]([A-Z])/g, (m) => m[0] + '_' + m[1]).toLowerCase();
    };
}
