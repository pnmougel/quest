const JSONStream = require('JSONStream');
import {createReadStream} from 'fs';

export class JsonUtils {
    static getTopObjects(file: string, maxObject: number): Promise<any[]> {
        return new Promise((resolve, reject) => {
            const objects: any[] = [];
            const stream = createReadStream(file, {flags: 'r', encoding: 'utf-8'})
                .pipe(JSONStream.parse('.'));
            stream.on('data', (data: any) => {
                if (objects.length < maxObject) {
                    objects.push(data);
                    if (objects.length >= maxObject) {
                        stream.destroy();
                        resolve(objects);
                    }
                }
            });
            stream.on("error", (err: any) => reject(err));
            stream.on("end", () => resolve(objects));
        })
    }
}
