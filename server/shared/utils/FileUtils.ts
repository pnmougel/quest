import {createReadStream} from "fs";

export class FileUtils {
    static readTopLines(file: string, maxLineCount: number) : Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            // Read only maxLineCount lines
            let stream = createReadStream(file, {flags: 'r', encoding: 'utf-8'});
            let lineCounter = 0;
            let data = '';
            stream.on("data", (moreData) => {
                data += moreData;
                lineCounter += data.split("\n").length - 1;
                if (lineCounter > maxLineCount + 1) {
                    stream.destroy();
                    // Remove invalid last line
                    resolve(data.split('\n').slice(0, maxLineCount));
                }
            });
            stream.on("error", (err) => reject(err));
            stream.on("end", () => resolve(data.split("\n")));
        });
    }
}
