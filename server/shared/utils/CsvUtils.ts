import {FileUtils} from './FileUtils';

const parse = require('csv-parse/lib/sync');
import {createReadStream} from 'fs';

export class CsvUtils {
    static detectCsvDelimiter(file: string, maxLineCount: number, delimiters = [',', ';', '\t', '|', ':']): Promise<string[]> {
        return FileUtils.readTopLines(file, maxLineCount).then(lines => {
            return new Promise(resolve => {
                const csvData = lines.join("\n");
                const validDelimiters = delimiters.filter(delimiter => {
                    let isValid = true;
                    // csv-parse throws error by default if the number of columns is inconsistent between lines
                    try {
                        const rows = parse(csvData, {delimiter});
                        isValid = rows.some((row: any[]) => row.length > 1);
                    } catch (e) {
                        isValid = false;
                    }
                    return isValid;
                });
                resolve(validDelimiters);
            });
        });
    }

    static getHeaders(file: string, delimiter:string): Promise<string[]> {
        return FileUtils.readTopLines(file, 1).then(lines => {
            if(lines.length !== 1) {
                return [];
            } else {
                return parse(lines[0], {delimiter})[0];
            }
        });
    }

    static getTopRows(file: string, delimiter:string, topRows: number): Promise<string[]> {
        return FileUtils.readTopLines(file, topRows + 1).then(lines => {
            let data = lines.shift();
            // @ts-ignore
            return parse(data.join('\n'), {delimiter});
        });
    }
}
