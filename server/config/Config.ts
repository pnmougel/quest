import {config as configDev} from './dev';
import {config as configTest} from './test';
import {config as configProd} from './prod';

export let config = configDev;
if (process.env['NODE_ENV'] === 'dev') {
    config = configDev;
} else if (process.env['NODE_ENV'] === 'production') {
    config = configProd;
} else if (process.env['NODE_ENV'] === 'preprod') {
    config = configTest;
}
