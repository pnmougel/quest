export const config = {
    "server": {
        "port": 3000,
        "host": "http://localhost:3000",
    },
    "front": {
        "host": "http://localhost:4200"
    }
};
