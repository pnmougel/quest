
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';

@Model()
export class SearchTemplate {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update'],
    })
    createdAt?: string;

    @Field()
    isPublic?: boolean;

    @Field({
        description: 'Name of the document template'
    })
    name?: string;

    @Field({
        description: 'Textual description of the search template'
    })
    description?: string;


    @Field({
        description: 'Name of the corresponding elasticsearch template'
    })
    esTemplateName?: string;

    @Field({
        description: 'Content of the search template'
    })
    template_content?: string;

    @Field()
    forIndices?: string;
}
