import {Request, Response} from 'express';
import {BaseController} from '../../BaseController';
import request from 'request-promise';
import {Db} from '../../shared/database/Db';
import 'reflect-metadata';
import {Environment} from './models/Environment';
import {Controller} from '../../shared/routing/ControllerDefinition';
import {DeleteRoute, GetRoute, PostRoute, PutRoute} from '../../shared/routing/RouteDefinition';
import {bodyParam} from '../../shared/routing/swagger/JsonBodyParam';
import {pathParam} from '../../shared/routing/swagger/PathParam';
import {queryParam} from '../../shared/routing/swagger/QueryParam';
import {AliveQuery} from './models/AliveQuery';

@Controller({
    name: 'Environment',
    basePath: '/environment',
    description: 'Environment endpoints',
})
export class EsEnvironmentController extends BaseController {
    @PostRoute('/', {
        name: 'Add',
        description: 'Register a new search environment',
        params: [bodyParam(Environment, 'add')],
    })
    async addEnvironment(req: Request, res: Response) {
        const environment: Environment = req.body;
        const stmt = Db.insert('environment', environment);
        res.send({id: stmt.lastInsertRowid});
    }

    @PutRoute('/:id', {
        name: 'Update',
        params: [
            bodyParam(Environment, 'update'),
            pathParam('id', 'Id of the environment to update', 'integer')],
    })
    async updateEnvironment(req: Request, res: Response) {
        const environment: Environment = req.body;
        const stmt = Db.update('environment', req.params.id, environment);
        res.send({
            'nb_entries_updated': stmt.changes
        });
    }

    @GetRoute('/search', {
        name: 'Search',
        description: 'List the environments'
    })
    async listEnvironments(req: Request, res: Response) {
        res.send(Db.select('environment'));
    }

    @GetRoute('/alive', {
        name: 'Test connection',
        description: 'Test is the connection corresponds to an elasticsearch cluster',
        params: [queryParam(AliveQuery)]
    })
    async checkEnvironment(req: Request, res: Response) {
        const url = `${req.query['protocol']}://${req.query['host']}:${req.query['port']}`;
        request.get(url).then(esResponse => {
            res.send({
                isAlive: true,
                info: JSON.parse(esResponse)
            });
            // return esResponse;
        }).catch(err => {
            res.send({
                isAlive: false,
                error: `Unable to connect to elasticsearch on url ${url}`
            });
        });
    }

    @GetRoute('/:id', {
        name: 'Get by id',
        description: 'Get an environment by its id'
    })
    async getEnvironment(req: Request, res: Response) {
        const entry = Db.get('environment', req.params['id']);
        if (!!entry) {
            res.send(entry);
        } else {
            res.status(404).send({
                message: `Entry with id ${req.params['id']} not found`
            });
        }
    }

    @DeleteRoute('/:id', {
        name: 'Remove',
        description: 'Remove an environment by its id'
    })
    async deleteEnvironment(req: Request, res: Response) {
        const stmt = Db.delete('environment', req.params['id']);
        res.send({
            'nb_entries_updated': stmt.changes
        });
    }
}
