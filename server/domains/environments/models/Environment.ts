import {Model} from '../../../shared/model/ModelDefinition';
import {Field} from '../../../shared/model/FieldDefinition';

export enum Protocol {
    Http = 'http',
    Https = 'https'
}

@Model({name: 'environment'})
export class Environment {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update']
    })
    createdAt?: string;

    @Field({
        description: 'Name of the environment'
    })
    name?: string;

    @Field({
        defaultValue: 'localhost'
    })
    host?: string;

    @Field({
        defaultValue: 9200,
        // validators: [(value, requestCtx) => {
        //     return value > 1024
        // }]
    })
    port?: number;

    @Field({
        enum: Protocol,
        defaultValue: Protocol.Http
    })
    protocol?: Protocol;

    @Field({
        defaultValue: [],
    })
    disabledEndpoints?: string[];

    @Field({
        required: false
    })
    theme?: string;
}
