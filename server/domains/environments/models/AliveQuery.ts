import {Model} from '../../../shared/model/ModelDefinition';
import {Field} from '../../../shared/model/FieldDefinition';
import {Protocol} from './Environment';

@Model({isTable: false})
export class AliveQuery {
    @Field()
    host?: string;

    @Field()
    port?: number;

    @Field({
        enum: Protocol
    })
    protocol?: Protocol;
}
