import {Request, Response} from 'express';
import {BaseController} from '../../BaseController';
import * as path from 'path';
import {ModelRegistry} from '../../shared/model/ModelRegistry';
import {Controller} from '../../shared/routing/ControllerDefinition';
import {ControllerRegistry} from '../../shared/routing/ControllerRegistry';
import {GetRoute} from '../../shared/routing/RouteDefinition';

@Controller({
    basePath: '/swagger',
    description: 'Swagger endpoints'
})
export class SwaggerController extends BaseController {
    private static info = {
        title: 'Quest API',
        version: '1.0.0',
        description: 'API for Quest application',
    };
    private static basePath = 'localhost:3000';

    @GetRoute('/')
    async getRedocTemplate(req: Request, res: Response) {
        res.sendFile(path.join(__dirname, '/redoc.html'));
    }

    @GetRoute('/swagger.json', {
        name: 'OpenAPI specification',
        description: 'OpenAPI specification'
    })
    async getSwaggerJson(req: Request, res: Response) {
        const controllers = ControllerRegistry.Instance.controllers;

        // Build paths
        const paths: any = {};
        controllers.forEach(controller => {
            controller.routes.forEach(route => {
                const path = controller.options.basePath + route.swaggerPath;
                if(!paths[path]) {
                    paths[path] = {};
                }
                const swaggerRouteSpec = route.swaggerSpec(controller);
                route.methods.forEach(method => {
                    paths[path][method.toLowerCase()] = swaggerRouteSpec;
                });
                // paths[path] = route.swaggerSpec(controller);
            });
        });

        // Build definitions
        const definitions: any = {};
        ModelRegistry.Instance.models.forEach(model => {
            definitions[`${model.name}`] = model.swaggerSchema('');
            model.operations.forEach(operation => {
                definitions[`${model.name}_${operation}`] = model.swaggerSchema(operation);
            });
        });

        // Build tags
        const tags = controllers.map(controller => {
            return {
                name: controller.options.name,
                description: controller.options.description
            };
        });

        // Build responses
        const responses: any = {};

        // Build parameters
        const parameters: any = {};

        // Build securityDefinitions
        const securityDefinitions: any = {};

        // this.swaggerSpecs.definitions = definitions;
        const spec = {
            info: SwaggerController.info,
            basePath: SwaggerController.basePath,
            swagger: '2.0',
            paths,
            definitions,
            responses,
            parameters,
            securityDefinitions,
            tags,
        };
        res.send(spec);
    }
}
