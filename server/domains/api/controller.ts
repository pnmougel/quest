import {Request, Response, Router} from 'express';
import {BaseController} from '../../BaseController';

import {EndpointDefinition, apiDefinition} from './definitions/api_definition'
import {tags} from './definitions/tags';
import {endpoints_info} from './definitions/endpoints_info';
import {Controller} from '../../shared/routing/ControllerDefinition';
import {GetRoute} from '../../shared/routing/RouteDefinition';

@Controller({
    basePath: '/api',
    name: 'Elasticsearch API',
    description: 'Elasticsearch Api endpoints'
})
export class EndpointsController extends BaseController {
    @GetRoute('/api', {
        name: 'Get api',
        description: 'Elasticsearch API definition'
    })
    async getApi(req: Request, res: Response) {
        res.send({});
    }

    @GetRoute('/endpoints', {
        name: 'Get endpoints',
        description: 'List Elasticsearch endpoints'
    })
    async listEndpoints(req: Request, res: Response) {
        let endpoints = Object.keys(apiDefinition.endpoints).map(api => {
            const base_api: string = api.split('.')[0];
            const apiInfo: EndpointDefinition = apiDefinition.endpoints[api];
            if(!!endpoints_info[base_api]) {
                apiInfo.description = endpoints_info[base_api].description;
                apiInfo.tags = endpoints_info[base_api].tags;
                delete apiInfo.url_params;
                delete apiInfo.data_autocomplete_rules;
                apiInfo.disabled = endpoints_info[base_api].disabled;
                if(!!endpoints_info[base_api].rename) {
                    // @ts-ignore
                    apiInfo.id = endpoints_info[base_api].rename;
                }
            }
            return apiInfo;
        }).filter(e => !e.disabled);

        res.send(endpoints)
    }

    @GetRoute('/endpoint_tags', {
        name: 'Endpoints Tags',
        description: 'Tags for elasticsearch endpoints'
    })
    async listEndpointTags(req: Request, res: Response) {
        res.send(tags);
    }

}
