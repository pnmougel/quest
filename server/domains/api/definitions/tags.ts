export const tags = [
        {
            label: 'Search',
            key: 'search',
            description: 'Endpoints to perform search',
            requiresLicense: false,
        },
        {
            label: 'Update',
            key: 'update',
            description: 'Update documents, indices or cluster states',
            requiresLicense: false,
        },
        {
            label: 'Read',
            key: 'read',
            description: 'Read only endpoints',
            requiresLicense: false,
        },
        {
            label: 'Delete',
            key: 'delete',
            description: 'Delete documents, indices or cluster states',
            requiresLicense: false,
        },
        {
            label: 'Analysis',
            key: 'analysis',
            description: 'Endpoints to analyze search request',
            requiresLicense: false,
        },
        {
            label: 'Cluster status',
            key: 'cluster_status',
            description: 'Retrieve cluster information',
            requiresLicense: false,
        },
        {
            label: 'Index admin',
            key: 'index_admin',
            description: 'Perform index administration tasks',
            requiresLicense: false,
        },
        {
            label: 'cluster admin',
            key: 'cluster_admin',
            description: 'Perform cluster administration tasks',
            requiresLicense: false,
        },
        {
            label: 'X-Pack',
            key: 'xpack',
            description: 'X-pack related endpoints',
            requiresLicense: false,
        },
        {
            label: 'Security',
            key: 'security',
            description: 'Manage cluster security',
            requiresLicense: true,
        },
        {
            label: 'Cross cluster replication',
            key: 'ccr',
            description: 'Manage cross cluster replication. Requires license',
            requiresLicense: true
        },
        {
            label: 'Machine Learning',
            key: 'ml',
            description: 'Machine learning features',
            requiresLicense: true,
        }
    ]
;
export default tags;
