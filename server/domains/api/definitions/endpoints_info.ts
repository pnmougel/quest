interface EndpointInfo {
  tags: string[];
  description: string;
  rename?: string;
  disabled?: boolean;
}

export const endpoints_info: {[s: string]: EndpointInfo } = {
  'bulk': {
    tags: ["update", "delete"],
    description: 'Batch operations to create, update or delete multiple documents'
  },
  '_search/template/{id}': {
    tags: ["read", "search"],
    description: 'Get a search template',
    rename: 'get_search_template'
  },
  'clear_scroll': {
    tags: ["search", "update"],
    description: 'Delete a search scroll context'
  },
  '_processor': {
    tags: [],
    description: '',
    disabled: true
  },
  'count': {
    tags: ["read"],
    description: 'Count documents matching a query'
  },
  'create': {
    tags: ["update"],
    description: ''
  },
  'delete_by_query': {
    tags: ["delete"],
    description: 'Remove documents matching a query'
  },
  'delete_script': {
    tags: ["delete"],
    description: 'Remove a script by id'
  },
  'delete': {
    tags: ["delete"],
    description: ''
  },
  'exists_source': {
    tags: ["read"],
    description: 'Check if the _source field exists on a document'
  },
  'exists': {
    tags: ["read"],
    description: ''
  },
  'explain': {
    tags: ["read", "analysis"],
    description: ''
  },
  'field_caps': {
    tags: ["read", "analysis"],
    description: ''
  },
  'get_script': {
    tags: ["read"],
    description: ''
  },
  'get_source': {
    tags: ["read"],
    description: ''
  },
  'get': {
    tags: ["read"],
    description: ''
  },
  'index': {
    tags: ["update"],
    description: 'Index a document'
  },
  'ssl': {
    tags: ["security", "cluster_admin", "read"],
    description: 'Retrieve information about the X.509 certificates that are used to encrypt communications in your Elasticsearch cluster'
  },
  'put_mapping': {
    tags: ["index_admin", "update"],
    description: 'Set or update index mappings'
  },
  'put_settings': {
    tags: ["cluster_admin"],
    description: ''
  },
  'info': {
    tags: ["read", "cluster_admin"],
    description: 'Get cluster information'
  },
  'monitoring': {
    tags: ["cluster_admin"],
    description: ''
  },
  'mget': {
    tags: ["read", "search"],
    description: 'Retrieve multiple documents by their ids'
  },
  'msearch_template': {
    tags: ["read", "search"],
    description: 'Perform multiple template search requests in a single query'
  },
  'msearch': {
    tags: ["read", "search"],
    description: 'Perform multiple search requests in a single query'
  },
  'mtermvectors': {
    tags: ["analysis"],
    description: 'Retrieve term information for fields in multiple documents'
  },
  'update': {
    tags: ["update"],
    description: 'Update or upsert a document by id'
  },
  'ping': {
    tags: ["cluster_admin"],
    description: 'Check if the cluster is alive',
    disabled: true
  },
  'put_script': {
    tags: ["update"],
    description: ''
  },
  'rank_eval': {
    tags: ["search", "analysis"],
    description: ''
  },
  'reindex_rethrottle': {
    tags: ["update", "cluster_admin"],
    description: 'Update the number of request per seconds for tasks',
    rename: 'rethrottle',
  },
  'reindex': {
    tags: ["update", "index_admin"],
    description: ''
  },
  'render_search_template': {
    tags: ["search", "analysis"],
    description: 'Render a search template using the provided parameters'
  },
  'scripts_painless_execute': {
    tags: ["update", "delete", "search"],
    description: 'Execute a painless script'
  },
  'scroll': {
    tags: ["read", "search"],
    description: ''
  },
  'search_shards': {
    tags: ["search", "analysis"],
    description: 'Returns the indices and shards that a search request would be executed against'
  },
  'search_template': {
    tags: ["read", "search"],
    description: 'Perform a search using a search template'
  },
  'search': {
    tags: ["read", "search"],
    description: ''
  },
  'update_by_query': {
    tags: ["update"],
    description: 'Update documents matching a query'
  },
  'termvectors': {
    tags: ["analysis"],
    description: 'Retrieve term information for fields in a document'
  },
  'tasks': {
    tags: ["cluster_admin"],
    description: ''
  },
  'nodes': {
    tags: ["cluster_admin"],
    description: ''
  },
  'ingest': {
    tags: ["cluster_admin"],
    description: ''
  },
  'snapshot': {
    tags: ["cluster_admin"],
    description: ''
  },
  'cluster': {
    tags: ["cluster_admin"],
    description: ''
  },
  'ccr': {
    tags: ["ccr"],
    description: ''
  },
  'ilm': {
    tags: ["index_admin"],
    description: 'Index lifecycle management'
  },
  'cat': {
    tags: ["cluster_admin", "cluster_status", "read"],
    description: ''
  },
  'security': {
    tags: ["security", "cluster_admin"],
    description: 'Manage cluster security'
  },
  'xpack': {
    tags: ["xpack"],
    description: ''
  },
  'indices': {
    tags: ["index_admin"],
    description: ''
  },
  'ml': {
    tags: ["ml"],
    description: ''
  },
};
export default endpoints_info;
