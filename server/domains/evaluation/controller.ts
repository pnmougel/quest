import {Request, Response, Router} from 'express';
import {BaseController} from '../../BaseController';
import {CsvUtils} from '../../shared/utils/CsvUtils';
import {Db} from '../../shared/database/Db';
import {FileUtils} from '../../shared/utils/FileUtils';
import {JsonUtils} from '../../shared/utils/JsonUtils';
import {createReadStream} from 'fs';
import {DatasetType} from '../../../front/src/app/domains/evaluation/models/DatasetType';
import {Evaluation} from './models/Evaluation';
import {Controller} from '../../shared/routing/ControllerDefinition';
import {GetRoute, PostRoute} from '../../shared/routing/RouteDefinition';
import {bodyParam, JsonBodyParam} from '../../shared/routing/swagger/JsonBodyParam';

const parse = require('csv-parse/lib/sync');
const JSONStream = require('JSONStream');

@Controller({
    name: 'Evaluation',
    basePath: '/evaluation',
    description: 'Endpoints related to evaluations'
})
export class EvaluationController extends BaseController {
    // router: Router = Router();
    //
    // constructor() {
    //     super();
    //     this.router
    //         .post('/', AddEvaluationValidation, this.validate(), this.asyncHandler((req, res) => this.addEvaluation(req, res)))
    //         // .post('/evaluation/:id/run', ComputeEvaluationValidation, this.validate(), this.asyncHandler((req, res) => this.computeEvaluation(req, res)))
    //         // .get('/evaluation/list', ListEvaluationValidation, this.validate(), this.asyncHandler((req, res) => this.listEvaluation(req, res)))
    //         // .get('/evaluation/:id/results', GetEvaluationResultsValidation, this.validate(), this.asyncHandler((req, res) => this.getEvaluationResult(req, res)))
    //         // .get('/evaluation/:id', GetEvaluationValidation, this.validate(), this.asyncHandler((req, res) => this.getEvaluation(req, res)))
    //         // .get('/evaluation/tasks/:id', GetEvaluationTaskValidation, this.validate(), this.asyncHandler((req, res) => this.getEvaluationTask(req, res)))
    //     ;
    // }

    @PostRoute('/', {
        name: 'Add',
        description: 'Register a new evaluation',
        params: [
            bodyParam(Evaluation, 'add')
        ]
    })
    async addEvaluation(req: Request, res: Response) {
        const stmt = Db.insert('evaluation', {
            name: req.body.name,
            description: req.body.description,
            is_public: 1,
            environment_id: req.body.environment_id,
            evaluation_dataset_id: req.body.evaluation_dataset_id,
            search_template: req.body.search_template,
            pretreatment_id: req.body.pretreatment_id,
            indices: req.body.indices
        });

        return res.send({
            status: 'created',
            id: stmt.lastInsertRowid
        });
    }

    @GetRoute('/search', {

    })
    async search(req: Request, res: Response) {

    }

    @GetRoute('/:id/results', {

    })
    async getResults(req: Request, res: Response) {

    }

    @GetRoute('/:id', {

    })
    async getById(req: Request, res: Response) {

    }

    @PostRoute('/:id/run', {
        name: 'Run',
        description: 'Run an evaluation'
    })
    async runEvaluation(req: Request, res: Response) {
        const evaluationId = req.params.evaluationId;
        const evaluation = Db.getAs(Evaluation, 'evaluation', evaluationId);

        const nbItemsRes = Db.count('evaluation_dataset_item', {
            evaluation_dataset_id: evaluation.evaluationDatasetId
        });
        console.log(nbItemsRes);

        Db.insert('evaluation_result', {
            evaluation_id: evaluationId,
            environment_id: evaluation.environmentId,
            status: 0,
            row_count: 0, // Use nbItemRes value :)
            row_computed: 0
        })

        // Create evaluation result entry

        // Foreach evaluation dataset items
        //    Run search template
        //    Compute results
        //    Create evaluation_result_item entry


    //     create table if not exists evaluation_result_item (
    //         id integer not null,
    //         evaluation_result_id integer not null,
    //         evaluation_dataset_item_id integer not null,
    //
    //         -- Position of the row. Maybe not useful
    //     row integer not null,
    //         -- Number of documents retrieved by the query
    //     total_results integer not null,
    //         -- Array of documents returned by the query containing :
    //         --   id
    //     --   score
    //     --   source (optionally)
    //     --   status : 0 => is_not_match, 1 => is_match, 2 => is_ignored
    //     document_ids_scores_json text not null,
    //         count_match integer not null,
    //         -- Position of the first matching document
    //     first_match_position integer,
    //         count_not_match integer not null,
    //         count_ignored integer not null,
    //         -- If true, all the ids that where required are matched
    //     is_all_match boolean not null,
    //         primary key (id)
    // );

        //
        // Endforeach
    }
}
