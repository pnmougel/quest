import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';

@Model({
    description: 'Object describing an evaluation'
})
export class Evaluation {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update'],
    })
    createdAt?: string;

    @Field({
        description: 'Name of the evaluation'
    })
    name?: string;

    @Field({
        required: false,
        description: 'Textual description of the evaluation'
    })
    description?: string;

    @Field({
        defaultValue: true,
    })
    isPublic?: boolean;

    @Field()
    evaluationDatasetId?: number;

    @Field()
    environmentId?: number;

    @Field({
        required: false,
    })
    pretreatmentId?: number;

    @Field({
        required: false,
        description: 'Name of the search template to use for the evaluation',
        validators: [(value, options) => {
            console.log('TODO: Validate the existence of the search template');
        }]
    })
    searchTemplate?: string;

    @Field({
        required: false,
        validators: [(value, options) => {
            console.log('TODO: Validate the existence of the indices');
        }]
    })
    indices?: string;
}
