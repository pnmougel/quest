export enum EvaluationResultStatus {
    Running,
    Ended,
    Error
}
