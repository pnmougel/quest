
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';
import {EvaluationResultStatus} from './EvaluationResultStatus';

@Model()
export class EvaluationResult {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update'],
    })
    createdAt?: string;

    @Field()
    evaluationId?: number;

    @Field()
    environmentId?: number;

    @Field({
        enum: EvaluationResultStatus,
    })
    status?: EvaluationResultStatus;

    @Field()
    rowCount?: number;

    @Field()
    rowComputed?: number;
}
