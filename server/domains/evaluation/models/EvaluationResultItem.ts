
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';
import {DocumentMatchStatus} from './DocumentMatchStatus';

export class DocumentMatch {
    id?: string;
    score?: number;
    status?: DocumentMatchStatus
}

@Model()
export class EvaluationResultItem {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field()
    evaluationResultId?: number;

    @Field()
    evaluationDatasetItemId?: number;

    @Field()
    row?: number;

    @Field()
    totalResults?: number;

    @Field({
        description: 'List of the documents matching the query'
    })
    documents?: DocumentMatch[];

    @Field()
    countMatch?: number;

    @Field({
        description: 'Position of the first matching document'
    })
    firstMatchPosition?: number;

    @Field()
    countNotMatch?: number;

    @Field()
    countIgnored?: number;

    @Field({
        description: 'If true, all the ids that where required are matched'
    })
    isAllMatch?: boolean;
}
