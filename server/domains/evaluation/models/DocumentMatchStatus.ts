export enum DocumentMatchStatus {
    Match,
    NotMatched,
    Ignored
}
