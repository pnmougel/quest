import {Request, Response, Router} from 'express';
import {BaseController} from '../../BaseController';
import {ModelRegistry} from '../../shared/model/ModelRegistry';
import {Db} from '../../shared/database/Db';
import {PostRoute} from '../../shared/routing/RouteDefinition';
import {Controller} from '../../shared/routing/ControllerDefinition';

@Controller({
    basePath: '/setup',
    description: 'Quest Database initialisation'
})
export class SetupController extends BaseController {
    @PostRoute('/init_db', {
        name: 'Init db',
        description: 'Initialize database'
    })
    async initDb(req: Request, res: Response) {
        const createdTables = new Set<string>();
        const handlingTables = new Set<string>();
        const errors: any[] = [];

        ModelRegistry.Instance.models.forEach(model => {
            try {
                SetupController.handleTable(model.tableName, createdTables, handlingTables, errors);
            } catch (e) {
                errors.push({
                    table: model.tableName,
                    message: e.toString()
                });
            }
        });
        res.send({
            tables: Array.from(createdTables),
            errors,
        });
    }

    private static handleTable(tableName: string, createdTables: Set<string>, handlingTables: Set<string>, errors: any[] = []) {
        if(createdTables.has(tableName)) {
            return;
        }
        const model = ModelRegistry.Instance.getModelForTable(tableName);
        if(!model) {
            throw new Error(`Missing model definition for table ${tableName}`)
        }
        handlingTables.add(tableName);
        model.dependencies.forEach(dependency => {
            if(handlingTables.has(dependency)) {
                throw new Error(`Circular dependency detected in table foreign key constraints (${tableName})`)
            }
            SetupController.handleTable(dependency, createdTables, handlingTables, errors);
        });
        try {
            Db.exec(model.sqlDropTableStmt);
        } catch (e) {
            errors.push({
                table: model.tableName,
                message: e.toString()
            });
        }
        try {
            Db.exec(model.sqlCreateTableStmt);
        } catch (e) {
            errors.push({
                table: model.tableName,
                message: e.toString()
            });
        }
        handlingTables.delete(tableName);
        createdTables.add(tableName);
    }
}

