import {Request, Response, Router} from 'express';
import {BaseController} from '../../BaseController';
import {CsvUtils} from '../../shared/utils/CsvUtils';
import {Db} from '../../shared/database/Db';
import {FileUtils} from '../../shared/utils/FileUtils';
import {JsonUtils} from '../../shared/utils/JsonUtils';
import {createReadStream} from 'fs';
import {DatasetType} from '../../../front/src/app/domains/evaluation/models/DatasetType';
import {EvaluationDatasetFile} from './models/EvaluationDatasetFile';
import {GetRoute, PostRoute} from '../../shared/routing/RouteDefinition';
import {bodyParam} from '../../shared/routing/swagger/JsonBodyParam';
import {Controller} from '../../shared/routing/ControllerDefinition';
import {AddFileDatasetQuery} from './queries/AddFileDatasetQuery';
import {queryParam} from '../../shared/routing/swagger/QueryParam';
import {FileDatasetSampleQuery} from './queries/FileDatasetSampleQuery';
import {pathParam} from '../../shared/routing/swagger/PathParam';
import {FormFieldType, formParam} from '../../shared/routing/swagger/FormParam';

const parse = require('csv-parse/lib/sync');
const JSONStream = require('JSONStream');

@Controller({
    basePath: '/evaluation_dataset',
    name: 'Evaluation datasets',
    description: `Endpoints related to evaluation dataset`,
})
export class EvaluationDatasetController extends BaseController {
    @PostRoute('/file', {
        name: 'Add file dataset',
        description: 'Create an evaluation dataset based on file',
    }, bodyParam(AddFileDatasetQuery))
    async addFileDataset(req: Request, res: Response) {
        const fileDatasetId = req.body['dataset_file_id'];
        const stmt = Db.insert('evaluation_dataset', {
            name: req.body.name,
            type: DatasetType.FILE,
            created_by: 0,
            is_public: 1
        });
        const evaluationDatasetId = stmt.lastInsertRowid;

        const dataset = Db.getAs(EvaluationDatasetFile, 'evaluation_dataset_file', fileDatasetId);
        if(!!dataset) {
            let response = null;
            const columnNames: string[] = req.body['column_names'];
            const targetIdColumns: string[] = req.body['target_ids']['column'];
            if(dataset.mimeType === 'application/json') {
                const stream = createReadStream(dataset.path as string, {flags: 'r', encoding: 'utf-8'})
                    .pipe(JSONStream.parse('.'));
                stream.on('data', (data: any) => {
                    const userQuery: any[] = [];
                    const targetIds: string[] = [];
                    columnNames.forEach((columnName: string) => {
                        userQuery.push(data[columnName])
                    });
                    targetIdColumns.forEach(targetIdColumn => {
                        targetIds.push(data[targetIdColumn])
                    });
                    Db.insert('evaluation_dataset_item', {
                        evaluation_dataset_id: evaluationDatasetId,
                        user_query_json: userQuery,
                        target_ids_json: targetIds
                    })
                });
                stream.on("error", (err: any) => (err));
                // stream.on("end", () => (objects));
            } else if(dataset.mimeType === 'text/csv') {

            }
        }

        res.send({
            status: 'created',
            id: evaluationDatasetId
        });
    }

    @PostRoute('/upload', {
        name: 'Upload dataset file',
        description: 'Upload a dataset file'
    }, formParam({
        name: 'dataset',
        description: 'dataset file',
        type: FormFieldType.File
        })
    )
    async uploadDatasetFile(req: Request, res: Response) {
        const fileInfo = req.files['dataset'];
        const stmt = Db.insert('evaluation_dataset_file', {
            path: fileInfo.tempFilePath,
            mime_type: fileInfo.mimetype
        });
        res.send({id: stmt.lastInsertRowid})
    }

    @GetRoute('/sample/:id', {
        name: 'Get sample',
        description: 'Return a parsed sample from the file dataset'
    }, queryParam(FileDatasetSampleQuery), pathParam('id', 'id of the file dataset', 'integer'))
    async getFileDatasetSample(req: Request, res: Response) {
        const dataset = Db.getAs(EvaluationDatasetFile, 'evaluation_dataset_file', req.params['datasetId']);
        const maxRows = !!req.query.max_rows ? parseInt(req.query.max_rows) : 20;
        if(!!dataset) {
            let response = null;
            if(dataset.mimeType === 'application/json') {
                try {
                    response = await EvaluationDatasetController.getJsonDataSample(dataset.path as string, maxRows);
                } catch (e) {
                    res.send({
                        error: 'Unable to parse json file'
                    })
                }
            } else if(dataset.mimeType === 'text/csv') {
                response = await EvaluationDatasetController.getCsvDataSample(dataset.path as string, maxRows, req.query.csv_delimiter);
            }
            if(!!response) {
                res.send(response);
            }
        }
    }

    @GetRoute('/:id/rows', {
        name: 'Get rows',
        description: 'Get the rows of an evaluation dataset'
    })
    async getDatasetRows(req: Request, res: Response) {
        // TODO: Add parameters from and count to limit the number of rows returned
        res.send([])
    }

    @GetRoute('/:id', {
        name: 'Get file dataset',
        description: 'Returns the evaluation dataset'
    })
    async getFileDatasetById(req: Request, res: Response) {
        res.send({});
    }

    @GetRoute('/search', {
        name: 'List dataset',
        description: 'List the evaluation dataset'
    })
    async listDataset(req: Request, res: Response) {
        res.send([]);
    }

    static async getJsonDataSample(file: string, maxRows: number) {
        const rows = await JsonUtils.getTopObjects(file, maxRows);
        // Join all the keys of the objects
        const headerSet = new Set<string>();
        const columns: string[] = [];
        rows.forEach(obj => {
            Object.keys(obj).forEach(key => {
                if(!headerSet.has(key)) {
                    headerSet.add(key);
                    columns.push(key);
                }
            })
        });
        return { columns, rows }
    }

    static async getCsvDataSample(file: string, maxRows: number, csvDelimiter: string) {
        if(!csvDelimiter) {
            const csvDelimiters = await CsvUtils.detectCsvDelimiter(file, 10);
            if(!csvDelimiters) {
                throw new Error('Unable to detect csv delimiter');
            } else {
                csvDelimiter = csvDelimiters[0];
            }
        }
        const data = await FileUtils.readTopLines(file, maxRows + 1);
        const rows = parse(data.join('\n'), {delimiter: csvDelimiter, columns: true});
        const columns = await CsvUtils.getHeaders(file, csvDelimiter);
        return {
            columns,
            rows,
            csvDelimiter,
        };
    }
}
