import {DatasetType} from '../../../front/src/app/domains/evaluation/models/DatasetType';
import {Db} from '../../shared/database/Db';
import {BaseDao} from '../../shared/database/BaseDao';
import fs from "fs";

export class EvaluationDatasetDao extends BaseDao {
    // create_environment = fs.readFileSync('./domains/evaluation_dataset/sql/create_environment.sql', 'utf-8');

    static addDataset(name: string, datasetType: DatasetType) {

    }

    /**
     * @param path
     * @param mimeType
     * @return the id of the inserted row
     */
    static addEvaluationDatasetFile(path: string, mimeType: string) {
        return Db.insert('evaluation_dataset_file', {
            path: path,
            mime_type: mimeType
        });
    }

    static updateEvaluationDatasetFile(datasetFileId: string, mimeType: string, csvSeparator: string) {

    }

}
