import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';

export enum FileDatasetFieldType {
    String,
    Number,
    Boolean,
    Array,
    Object,
}

@Model({
    isTable: false
})
export class FileDatasetFieldDescriptor {
    @Field({
        description: 'Name of the source field'
    })
    sourceField?: string;

    @Field({
        description: 'Name of the target field. If specified, `sourceField` will be renamed to the value of `targetField`',
        required: false
    })
    targetField?: string;

    @Field({
        description: 'Type of the field',
        enum: FileDatasetFieldType
    })
    type?: FileDatasetFieldType;

    @Field({
        description: 'Define the array separator for csv file and fieldType Array',
        required: false
    })
    arraySeparator?: string;
}

@Model({
    isTable: false
})
export class AddFileDatasetQuery {
    @Field({
        description: 'Name of the dataset'
    })
    name?: string;

    @Field({
        description: 'Description of the dataset',
        required: false
    })
    description?: string;

    @Field({
        description: 'Is dataset public',
        defaultValue: true
    })
    isPublic?: boolean;

    @Field({
        description: 'Descriptors of the fields from the dataset file',
        arrayItemsType: {
            '$ref': '#/definitions/FileDatasetFieldDescriptor'
        }
    })
    fieldDescriptors?: FileDatasetFieldDescriptor[];

    @Field()
    evaluationDatasetFileId?: number;
}
