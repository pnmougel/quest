import {Model} from '../../../shared/model/ModelDefinition';
import {Field} from '../../../shared/model/FieldDefinition';

@Model({
    isTable: false
})
export class FileDatasetSampleQuery {
    @Field({
        description: 'The csv separator to use',
        required: false,
    })
    delimiter?: string;

    @Field({
        description: 'Maximum number of rows to retrieve',
        defaultValue: 20
    })
    maxRows?: number;
}
