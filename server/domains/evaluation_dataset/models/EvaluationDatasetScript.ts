
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';

@Model()
export class EvaluationDatasetScript {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        description: 'Id of the evaluation dataset'
    })
    evaluationDatasetId?: number;

    @Field({
        description: 'Script to run to build the evaluation dataset'
    })
    script?: string;
}
