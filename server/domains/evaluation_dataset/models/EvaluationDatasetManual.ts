
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';

@Model()
export class EvaluationDatasetManual {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update'],
    })
    createdAt?: string;

    @Field({
        description: 'Id of the evaluation dataset'
    })
    evaluationDatasetId?: number;

    @Field({
        description: 'Id of a document template'
    })
    documentTemplateId?: number;


    @Field({
        description: 'Elasticsearch index'
    })
    esIndex?: string;

    @Field({
        description: 'Id of the search template to use'
    })
    searchTemplateId?: number;

    @Field({
        description: 'Id of pretreatment to use'
    })
    pretreatmentId?: number;
}
