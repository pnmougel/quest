import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';


@Model({
    description: 'Describe a user query and the expected document ids'
})
export class EvaluationDatasetItem {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field()
    userQuery?: string[];

    @Field()
    targetIds?: string[];

    @Field()
    evaluationDatasetId?: number;
}
