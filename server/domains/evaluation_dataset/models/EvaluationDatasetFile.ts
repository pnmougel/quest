
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';

@Model()
export class EvaluationDatasetFile {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update'],
    })
    createdAt?: string;

    @Field({
        description: 'Path to the file'
    })
    path?: string;

    @Field({
        description: 'Mime type of the file'
    })
    mimeType?: string;
}
