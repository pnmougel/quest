
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';
import {DatasetType} from '../../../../front/src/app/domains/evaluation/models/DatasetType';

@Model()
export class EvaluationDataset {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update'],
    })
    createdAt?: string;

    @Field({
        description: 'Name of the evaluation dataset'
    })
    name?: string;

    @Field({
        required: false,
        description: 'Textual description of the evaluation dataset'
    })
    description?: string;

    @Field({
        defaultValue: true,
    })
    isPublic?: boolean;

    @Field({
        required: true,
        enum: DatasetType,
    })
    type?: DatasetType;

    @Field({
        required: true,
        description: 'List of the fields of the dataset'
    })
    userFields?: string[]
}
