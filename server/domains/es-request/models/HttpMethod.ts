export enum HttpMethod {
    Get = 'Get',
    Post = 'Post',
    Put = 'Put',
    Delete = 'Delete',
    Head = 'Head',
}
