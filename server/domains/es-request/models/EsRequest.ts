import {Field} from '../../../shared/model/FieldDefinition';
import {HttpMethod} from './HttpMethod';
import {Model} from '../../../shared/model/ModelDefinition';

@Model()
export class EsRequest {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update']
    })
    createdAt?: string;

    @Field()
    environmentId?: number;

    @Field({
        enum: HttpMethod
    })
    method?: HttpMethod;

    @Field()
    body?: string;

    @Field()
    path?: string;

    @Field({
        required: false
    })
    api?: string;
}
