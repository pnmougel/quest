import {Request, Response} from 'express';
import {BaseController} from '../../BaseController';
import {EsRequest} from './models/EsRequest';
import request from 'request-promise';
import {PostRoute} from '../../shared/routing/RouteDefinition';
import {bodyParam, JsonBodyParam} from '../../shared/routing/swagger/JsonBodyParam';
import {Controller} from '../../shared/routing/ControllerDefinition';

@Controller({
    basePath: '/request',
})
export class EsRequestController extends BaseController {
    @PostRoute('', {
        name: 'Send Request',
        description: 'Send a request to elasticsearch',
        params: [bodyParam(EsRequest)]
    })
    async sendEsRequest(req: Request, res: Response) {
        const esRequest: EsRequest = req.body;
        const env = req.esEnvironment;
        const uri: string = `${env.protocol}://${env.host}:${env.port}/${esRequest.path}`;
        // @ts-ignore
        const esResponse = await request(uri, {
            headers: { 'Content-Type': 'application/json' },
            method: esRequest.method,
            body: esRequest.body,
            resolveWithFullResponse: true,
            simple: false
        });

        let response = {
            es_status: esResponse.statusCode,
            es_body: esResponse.body,
            is_json_body: false,
        };
        try {
            response.es_body = JSON.parse(esResponse.body);
            response.is_json_body = true;
        } catch (e) {
            response.es_body = esResponse.body;
        }
        res.send(response);
    }
}
