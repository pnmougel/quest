import {checkSchema, check, validationResult, body} from 'express-validator/check';
import {EnvParamValidator} from '../../shared/validators/validator';

export const RequestValidation = [
    body("method").isIn(['GET', 'POST', 'PUT', 'DELETE', 'HEAD']),
    body("environment").custom(EnvParamValidator),
    body("path").not().isEmpty(),
    body("api").not().isEmpty(),
];


