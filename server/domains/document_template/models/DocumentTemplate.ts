
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';

@Model()
export class DocumentTemplate {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update'],
    })
    createdAt?: string;

    @Field()
    isPublic?: boolean;

    @Field({
        description: 'Name of the document template'
    })
    name?: string;

    @Field({
        description: 'Mustache content of the document template'
    })
    template?: string;

    @Field()
    forIndices?: string;
}
