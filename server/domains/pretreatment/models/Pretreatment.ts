
import {Field} from '../../../shared/model/FieldDefinition';
import {Model} from '../../../shared/model/ModelDefinition';

@Model()
export class Pretreatment {
    @Field({
        notIn: ['add', 'update']
    })
    id?: number;

    @Field({
        notIn: ['add', 'update'],
    })
    createdAt?: string;

    name?: string;
}
