import express from 'express';
import {config} from './config/Config';

import bodyParser from 'body-parser';
import {EsEnvironmentController} from './domains/environments/controller';
import {CorsHandler} from './handlers/cors_handler';
import {ErrorHandler} from './handlers/error_handler';
import {EsRequestController} from './domains/es-request/controller';
import {EndpointsController} from './domains/api/controller';
import {SwaggerController} from './domains/swagger/controller';
import {EvaluationDatasetController} from './domains/evaluation_dataset/controller';
import {EvaluationController} from './domains/evaluation/controller';
import {SetupController} from './domains/setup/controller';
import {Environment} from './domains/environments/models/Environment';
import {ModelRegistry} from './shared/model/ModelRegistry';
import {Evaluation} from './domains/evaluation/models/Evaluation';
import {EvaluationDataset} from './domains/evaluation_dataset/models/EvaluationDataset';
import {EvaluationDatasetItem} from './domains/evaluation_dataset/models/EvaluationDatasetItem';
import {EvaluationDatasetFile} from './domains/evaluation_dataset/models/EvaluationDatasetFile';
import {EvaluationResult} from './domains/evaluation/models/EvaluationResult';
import {EvaluationResultItem} from './domains/evaluation/models/EvaluationResultItem';
import {EsRequest} from './domains/es-request/models/EsRequest';
import {User} from './domains/users/models/User';
import {SearchTemplate} from './domains/search_template/models/SearchTemplate';
import {DocumentTemplate} from './domains/document_template/models/DocumentTemplate';
import {Pretreatment} from './domains/pretreatment/models/Pretreatment';
import {EvaluationDatasetManual} from './domains/evaluation_dataset/models/EvaluationDatasetManual';
import {EvaluationDatasetScript} from './domains/evaluation_dataset/models/EvaluationDatasetScript';
import {ControllerRegistry} from './shared/routing/ControllerRegistry';

const fileUpload = require('express-fileupload');
require('swagger-ui-express');

const app: express.Application = express();

declare global {
    namespace Express {
        interface Request {
            esEnvironment: Environment,
            files?: any;
            _validationErrors?: any[];
        }
    }
}

// Force annotation loading
ModelRegistry.Instance.registerModel([
    Environment,
    Evaluation,
    EvaluationDataset,
    EvaluationDatasetItem,
    EvaluationDatasetFile,
    EvaluationResult,
    EvaluationResultItem,
    EsRequest,
    User,
    SearchTemplate,
    DocumentTemplate,
    Pretreatment,
    EvaluationDatasetManual,
    EvaluationDatasetScript,
]);

ControllerRegistry.Instance.registerController([
    EsEnvironmentController,
    EsRequestController,
    EndpointsController,
    EvaluationDatasetController,
    EvaluationController,
    SetupController,
    SwaggerController,
]);

// Enable CORS
app.use(bodyParser.json({limit: '50mb'}));
app.use(fileUpload({
    useTempFiles : true,
    tempFileDir : '/tmp/'
}));
app.use(CorsHandler);
app.use(ErrorHandler);

// Register all routes
app.use('/', ControllerRegistry.Instance.getRouter());

app.listen(config.server.port, () => {
    console.log(`Listening at http://localhost:${config.server.port}/`);
});
