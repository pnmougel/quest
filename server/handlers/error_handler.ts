import {Request, Response, NextFunction, RequestHandler, ErrorRequestHandler} from 'express';

export const ErrorHandler: ErrorRequestHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
    res.status(500).send(err);
};
