import {NextFunction, Request, RequestHandler, Response} from 'express';
import {validationResult} from 'express-validator/check';

export class BaseController {
    asyncHandler(fn: RequestHandler) {
        return (req: Request, res: Response, next: NextFunction) => {
            const promise = fn(req, res, next);
            if(!!promise) {
                promise.catch(next)
            }
        };
    }

    validate(): RequestHandler {
        return function(req: Request, res: Response, next: NextFunction) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ errors: errors.array() });
            }
            next();
        };
    }
}
