

```{{([\w]+)}}{{\^(\1)}}(.*?(?=)){{\/(\1)}}``` regexp to match default values in mustache templates

E.g. 
```{{min_rating}}{{^min_rating}}bla fsdf {dsfsd {{/min_rating}}```
