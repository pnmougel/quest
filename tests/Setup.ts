import {existsSync, readdirSync, readFileSync} from 'fs';
import request from 'request-promise';

let datasets: string[] = [];

if(process.argv.length > 2) {
    datasets = process.argv.slice(2)
} else {
    datasets = readdirSync(`${__dirname}/data`);
}

datasets.forEach(async datasetName => {
    await buildDataset(datasetName);
});

function getMissingFile(basePath: string, requiredFiles: string[]) {
    requiredFiles.unshift('');
    return requiredFiles.find(requiredFile => {
        return !existsSync(`${basePath}/${requiredFile}`);
    });
}

async function buildDataset(datasetName: string) {
    console.log(`Building dataset ${datasetName}`);
    const basePath = `${__dirname}/data/${datasetName}`;
    const missingFile = getMissingFile(basePath, ['config.json', 'data.json', 'index_settings.json', 'index_mapping.json']);
    if(missingFile === '') {
        console.error(`Missing dataset ${datasetName}`);
        return;
    }
    if(!!missingFile) {
        console.error(`Missing file ${missingFile} for dataset ${datasetName}`);
        return;
    }
    const config = JSON.parse(readFileSync(`${basePath}/config.json`, 'UTF-8'));
    const host = config.default_connection;
    const data: object[] = JSON.parse(readFileSync(`${basePath}/data.json`, 'UTF-8'));
    const settings = JSON.parse(readFileSync(`${basePath}/index_settings.json`, 'UTF-8'));
    const mappings = JSON.parse(readFileSync(`${basePath}/index_mapping.json`, 'UTF-8'));

    // Delete index
    if(await indexExists(host, config.index)) {
        console.log(`Removing index ${config.index}`);
        await request.delete(`${host}/${config.index}`)
    }

    // Setup index
    const res = await request.put(`${host}/${config.index}`, { resolveWithFullResponse: true, json: true, body: {
            settings,
            mappings
        }
    }).promise();
    if(res.statusCode === 200)  {
        console.log(`Index ${config.index} created`);
    }
    // Index data
    data.forEach(async (doc, idx) => {
        await request.post(`${host}/${config.index}/_doc/${idx}`, { json: true, body: doc }).promise();
    });

    // Register search template
    if(existsSync(`${basePath}/search_templates`)) {
        readdirSync(`${basePath}/search_templates`).forEach(async searchTemplateFile => {
            const searchTemplateName = searchTemplateFile.replace('.json', '');
            console.log(searchTemplateName);
            const searchTemplateContent = readFileSync(`${basePath}/search_templates/${searchTemplateFile}`, 'UTF-8');
            // console.log(searchTemplateContent);
            const body = {
                script: {
                    lang: "mustache",
                    source: searchTemplateContent
                }
            };
            await request.post(`${host}/_scripts/${searchTemplateName}`, { json: true, body }).promise();
        })
    }
}


async function indexExists(host: string, indexName: string) {
    let res = await request.head(`${host}/${indexName}`, {
        simple: false,
        resolveWithFullResponse: true
    });
    return res.statusCode === 200;
}

async function registerTemplates() {

}
