import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './core/routing/app-routing.module';
import {App} from './core/views/main/main';
import {EnvironmentsService} from './domains/environments/services/environments.service';
import {RequestAutocomplete} from './domains/autocomplete/services/autocomplete.service';
import {QueryBuilder} from './domains/query_builder/views/query-builder';
import {RequestEditor} from './domains/query_builder/components/request_editor/request-editor';
import { AngularSplitModule } from 'angular-split';
import {Environments} from './domains/environments/views/search/Environments';
import {EsResponse} from './domains/query_builder/components/es_response/es-response';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import {EnvironmentInfoService} from './domains/environments/services/EnvironmentInfoService';
import {IndicesInput} from './domains/query_builder/components/indices_input/indices-input';
import {ApiInput} from './domains/query_builder/components/api_input/api-input';
import {ApiService} from './domains/api/services/api.service';
import {PathParamInput} from './domains/query_builder/components/path_param_input/pathParamInput';
import {FieldAutocompleteService} from './domains/autocomplete/services/fieldAutocomplete.service';
import {EnvironmentSelect} from './domains/environments/components/environmentSelect/environment-select';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AdditEnvironment} from './domains/environments/views/addit/AdditEnvironment';
import {HttpConfigInterceptor} from './core/HttpConfigInterceptor';
import {ToastrModule, ToastNoAnimationModule, ToastNoAnimation} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Evaluations} from './domains/evaluation/views/search/Evaluations';
import {AdditEvaluation} from './domains/evaluation/views/addit/AdditEvaluation';
import {EvaluationService} from './domains/evaluation/services/EvaluationService';
import {DatasetService} from './domains/evaluation/services/DatasetService';

@NgModule({
  declarations: [
    App,

    // Views
    QueryBuilder,
    AdditEnvironment,
    Environments,
    Evaluations,
    AdditEvaluation,

    // Components
    RequestEditor,
    EsResponse,
    IndicesInput,
    ApiInput,
    PathParamInput,
    EnvironmentSelect,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularSplitModule.forRoot(),
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    NgbModule,
    ToastNoAnimationModule.forRoot(),
  ],
  providers: [
    EnvironmentsService,
    EnvironmentInfoService,
    ApiService,
    RequestAutocomplete,
    FieldAutocompleteService,

    // Evaluations
    EvaluationService,
    DatasetService,

    {
      provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true
    }
  ],
  bootstrap: [App]
})
export class AppModule {
}
