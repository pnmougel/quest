import {EsRequestContext} from '../../query_builder/models/EsRequestContext';
import {EnvironmentInfoService} from '../../environments/services/EnvironmentInfoService';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AutocompleteFieldValue, AutocompleteFieldValueTag, FieldAutocompletion} from '../services/fieldAutocomplete.service';
import {EnvironmentsService} from '../../environments/services/environments.service';

export class IndexFieldAutocompletion implements FieldAutocompletion {
  name = 'indices';
  isMultiValued = true;

  constructor() {

  }

  getAutocompleteValues(requestCtx: EsRequestContext, envInfo: EnvironmentInfoService): Observable<AutocompleteFieldValue[]> {
    const indices$: Observable<AutocompleteFieldValue[]> = envInfo.getIndexDefinitions().pipe(map(v => {
      return v.map(e => {
        const tags: AutocompleteFieldValueTag[] = [{
          value: 'index',
          className: 'tag-warning'
        }];
        // health: e.health,
        if(e.status === 'closed') {
          tags.push({
            value: 'closed',
            className: 'tag-warning'
          })
        }
        return {
          value: e.index,
          tags,
          description: `${e.docs_count} documents`
        };
      });
    }));
    const aliases$: Observable<AutocompleteFieldValue[]> = envInfo.getAliasDefinitions().pipe(map(v => {
      return v.map(e => {
        const tags: AutocompleteFieldValueTag[] = [{
          value: 'alias',
          className: 'tag-info'
        }];
        return {
          value: e.index,
          tags,
          description: `Alias of index ${e.index}`
        };
      });
    }));

    return forkJoin(aliases$, indices$).pipe(map(values => {
      const [aliases, indices] = values;
      return aliases.concat(indices);
    }));
  }
}
