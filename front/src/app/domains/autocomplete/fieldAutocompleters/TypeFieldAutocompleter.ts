import {EsRequestContext} from '../../query_builder/models/EsRequestContext';
import {EnvironmentInfoService} from '../../environments/services/EnvironmentInfoService';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AutocompleteFieldValue, FieldAutocompletion} from '../services/fieldAutocomplete.service';

export class TypeFieldAutocompletion implements FieldAutocompletion {
  name = 'type';
  isMultiValued = false;

  constructor() {}

  getAutocompleteValues(requestCtx: EsRequestContext, envInfo: EnvironmentInfoService): Observable<AutocompleteFieldValue[]> {
    let indices = '_all';

    return envInfo.getTypesForIndices(indices).pipe(map(v => {
      return v.map(e => {
        return {
          value: e.type,
        };
      });
    }));
  }
}
