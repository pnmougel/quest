/*
 * Licensed to Elasticsearch B.V. under one or more contributor
 * license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright
 * ownership. Elasticsearch B.V. licenses this file to you under
 * the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import _ from 'lodash';

import API_DEFINITION from './definitions/api_definition'

import { UrlPatternMatcher } from './components/index';
import { UrlParams } from './core/url_params';
import  { globalsOnlyAutocompleteComponents, compileBodyDescription } from './core/body_completer';
import ParametrizedComponentFactories from './core/parametrized_component_factories';
import {Injectable} from '@angular/core';

@Injectable()
export class ApiAutocomplete {
  globalRules = {};
  endpoints = {};
  urlPatternMatcher: UrlPatternMatcher;
  name: string;
  globalBodyComponentFactories: ParametrizedComponentFactories;

  /**
   *
   * @param urlParametrizedComponentFactories a dictionary of factory functions
   * that will be used as fallback for parametrized path part (i.e., {indices} )
   * see UrlPatternMatcher
   */
  constructor() {
    const json = API_DEFINITION;
    this.globalRules = {};
    this.endpoints = {};
    this.urlPatternMatcher = new UrlPatternMatcher(new ParametrizedComponentFactories());
    this.globalBodyComponentFactories = new ParametrizedComponentFactories();
    this.name = '';

    const names = [];
    _.each(json, (apiJson, name) => {
      names.unshift(name);
      _.each(apiJson.globals || {}, (globalJson, globalName) => {
        this.addGlobalAutocompleteRules(globalName, globalJson);
      });
      _.each(apiJson.endpoints || {}, (endpointJson, endpointName) => {
        this.addEndpointDescription(endpointName, endpointJson);
      });
    });
    this.name = names.join(',');
  }

  addEndpointDescription(endpoint, description) {
    const copiedDescription: any = {};
    _.extend(copiedDescription, description || {});
    _.defaults(copiedDescription, {
      id: endpoint,
      patterns: [endpoint],
      methods: ['GET']
    });
    _.each(copiedDescription.patterns, (p) => {
      this.urlPatternMatcher.addEndpoint(p, copiedDescription);
    }, this);

    copiedDescription.paramsAutocomplete = new UrlParams(copiedDescription.url_params);
    copiedDescription.bodyAutocompleteRootComponents = compileBodyDescription(
      copiedDescription.id, copiedDescription.data_autocomplete_rules, this.globalBodyComponentFactories);

    this.endpoints[endpoint] = copiedDescription;
  };

  addGlobalAutocompleteRules(parentNode, rules) {
    this.globalRules[parentNode] = compileBodyDescription(
      'GLOBAL.' + parentNode, rules, this.globalBodyComponentFactories);
  };

  getGlobalAutocompleteComponents(term, throwOnMissing) {
    const result = this.globalRules[term];
    if (_.isUndefined(result) && (throwOnMissing || _.isUndefined(throwOnMissing))) {
      throw new Error('failed to resolve global components for  [\'' + term + '\']');
    }
    return result;
  };

  getEndpointDescriptionByEndpoint(endpoint) {
    return this.endpoints[endpoint];
  };


  getTopLevelUrlCompleteComponents(method){
    return this.urlPatternMatcher.getTopLevelComponents(method);
  };

  getUnmatchedEndpointComponents() {
    return globalsOnlyAutocompleteComponents();
  };

  getEndpointBodyCompleteComponents(endpoint) {
    const desc = getEndpointDescriptionByEndpoint(endpoint);
    if (!desc) {
      throw new Error('failed to resolve endpoint [\'' + endpoint + '\']');
    }
    return desc.bodyAutocompleteRootComponents;
  }

  clear() {
    this.endpoints = {};
    this.globalRules = {};
  };
}

export function getUnmatchedEndpointComponents() {
  return ACTIVE_API.getUnmatchedEndpointComponents();
}

export function getEndpointDescriptionByEndpoint(endpoint) {
  return ACTIVE_API.getEndpointDescriptionByEndpoint(endpoint);
}

export function getEndpointBodyCompleteComponents(endpoint) {
  return ACTIVE_API.getEndpointBodyCompleteComponents(endpoint)
}

export function getTopLevelUrlCompleteComponents(method) {
  return ACTIVE_API.getTopLevelUrlCompleteComponents(method);
}

export function getGlobalAutocompleteComponents(term, throwOnMissing) {
  return ACTIVE_API.getGlobalAutocompleteComponents(term, throwOnMissing);
}

let ACTIVE_API = new ApiAutocomplete();
