/*
 * Licensed to Elasticsearch B.V. under one or more contributor
 * license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright
 * ownership. Elasticsearch B.V. licenses this file to you under
 * the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import ace from 'brace/index';


const MatchingBraceOutdent = require('ace-code-editor/lib/ace/mode/matching_brace_outdent').MatchingBraceOutdent;
const TextMode = ace.acequire('ace/mode/text').Mode;
const ScriptMode = require('./script').ScriptMode;

const CstyleBehaviour = ace.acequire('ace/mode/behaviour/cstyle').CstyleBehaviour;
const CStyleFoldMode = require('ace-code-editor/lib/ace/mode/folding/cstyle').FoldMode;
const AceTokenizer = ace.acequire('ace/tokenizer').Tokenizer;
const InputHighlightRules = require('./input_highlight_rules').InputHighlightRules;
const WorkerClient = ace.acequire("ace/worker/worker_client").WorkerClient;

export default class InputMode extends TextMode {
  constructor() {
    super();
    this.$tokenizer = new AceTokenizer(new InputHighlightRules().getRules());
    this.$outdent = new MatchingBraceOutdent();
    this.$behaviour = new CstyleBehaviour();
    this.foldingRules = new CStyleFoldMode();
    this.createModeDelegates({
      'script-': ScriptMode
    });
  }

  getNextLineIndent(state, line, tab) {
    let indent = this.$getIndent(line);

    if (state !== 'string_literal') {
      const match = line.match(/^.*[\{\(\[]\s*$/);
      if (match) {
        indent += tab;
      }
    }

    return indent;
  };


  getCompletions() {
    // autocomplete is done by the autocomplete module.
    return [];
  };

  checkOutdent(state, line, input) {
    return this.$outdent.checkOutdent(line, input);
  };

  autoOutdent(state, doc, row) {
    this.$outdent.autoOutdent(doc, row);
  };

  createWorker(session) {
    const worker = new WorkerClient(["ace"], require("brace/worker/json"), "JsonWorker");
    worker.attachToDocument(session.getDocument());

    worker.on("annotate", function(e) {
      session.setAnnotations(e.data);
    });

    worker.on("terminate", function() {
      session.clearAnnotations();
    });

    return worker;
  };
}
