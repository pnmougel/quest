import {
  FieldAutocompleteComponent, IdAutocompleteComponent,
  IndexAutocompleteComponent, ListComponent, TemplateAutocompleteComponent,
  TypeAutocompleteComponent,
  UsernameAutocompleteComponent
} from '../components/index';
import {Injectable} from '@angular/core';

@Injectable()
export default class ParametrizedComponentFactories {
  constructor() {}

  static isNotAnIndexName(name) {
    return name[0] === '_' && name !== '_all';
  }

  static idAutocompleteComponentFactory(name, parent, multi) {
    return new IdAutocompleteComponent(name, parent, multi);
  };

  getComponent(name, parent, provideDefault) {
    if (this[name]) {
      return this[name];
    } else if (provideDefault) {
      return ParametrizedComponentFactories.idAutocompleteComponentFactory;
    }
  }

  index(name, parent) {
    if (ParametrizedComponentFactories.isNotAnIndexName(name)) return;
    return new IndexAutocompleteComponent(name, parent, false);
  }

  indices(name, parent) {
    if (ParametrizedComponentFactories.isNotAnIndexName(name)) return;
    return new IndexAutocompleteComponent(name, parent, true);
  }

  type(name, parent) {
    return new TypeAutocompleteComponent(name, parent, false);
  }

  types(name, parent) {
    return new TypeAutocompleteComponent(name, parent, true);
  }

  id(name, parent) {
    return ParametrizedComponentFactories.idAutocompleteComponentFactory(name, parent, false);
  }

  transform_id(name, parent) {
    return ParametrizedComponentFactories.idAutocompleteComponentFactory(name, parent, false);
  }

  username(name, parent) {
    return new UsernameAutocompleteComponent(name, parent);
  }

  user(name, parent) {
    return new UsernameAutocompleteComponent(name, parent);
  }

  template(name, parent) {
    return new TemplateAutocompleteComponent(name, parent);
  }

  task_id(name, parent) {
    return ParametrizedComponentFactories.idAutocompleteComponentFactory(name, parent, false);
  }

  ids(name, parent) {
    return ParametrizedComponentFactories.idAutocompleteComponentFactory(name, parent, true);
  }

  fields(name, parent) {
    return new FieldAutocompleteComponent(name, parent, true);
  }

  field(name, parent) {
    return new FieldAutocompleteComponent(name, parent, false);
  }

  nodes(name, parent) {
    return new ListComponent(
      name,
      ['_local', '_master', 'data:true', 'data:false', 'master:true', 'master:false'],
      parent
    );
  }

  node(name, parent) {
    return new ListComponent(name, [], parent, false);
  }
};
