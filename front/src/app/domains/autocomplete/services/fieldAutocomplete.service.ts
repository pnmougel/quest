import {Injectable} from '@angular/core';
import {EsRequestContext} from '../../query_builder/models/EsRequestContext';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {EnvironmentInfoService} from '../../environments/services/EnvironmentInfoService';
import {IndexFieldAutocompletion} from '../fieldAutocompleters/IndexFieldAutocompletion';
import {TypeFieldAutocompletion} from '../fieldAutocompleters/TypeFieldAutocompleter';

export interface AutocompleteFieldValue {
  value: string;
  tags?: AutocompleteFieldValueTag[];
  description?: string;
}

export interface AutocompleteFieldValueTag {
  value: string;
  className?: string;
}

export interface FieldAutocompletion {
  name: string;
  getAutocompleteValues(requestCtx: EsRequestContext, envInfo: EnvironmentInfoService): Observable<AutocompleteFieldValue[]>;
  readonly isMultiValued: boolean;
}


export class DefaultFieldAutocompletion implements FieldAutocompletion {
  name = 'default';

  constructor(public readonly isMultiValued: boolean) {}

  getAutocompleteValues(requestCtx: EsRequestContext, envInfo: EnvironmentInfoService): Observable<AutocompleteFieldValue[]> {
    return new Observable<any[]>()
  }
}


@Injectable()
export class FieldAutocompleteService {
  autocompleters = new Map<string, FieldAutocompletion>()
  defaultAutocompleter = new DefaultFieldAutocompletion(false);

  constructor() {
    this.autocompleters.set('indices', new IndexFieldAutocompletion());
    this.autocompleters.set('type', new TypeFieldAutocompletion());
  }

  getAutocompleter(name: string): FieldAutocompletion {
    return this.autocompleters.has(name) ? this.autocompleters.get(name) : this.defaultAutocompleter
  }
}
