export interface ConnectionUrl {
  protocol: string;
  host: string;
  port: number;
}
