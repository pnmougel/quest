export class Environment {
  constructor(
    public readonly id: number,
    public readonly name: string,
    public readonly protocol: string,
    public readonly host: string,
    public readonly port: number,
    public readonly disabled_endpoints?: string[],
    public readonly theme?: string,
  ) {
  }

  get url() {
    return `${this.protocol}://${this.host}:${this.port}`;
  }
}
