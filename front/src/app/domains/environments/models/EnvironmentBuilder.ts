import {ConnectionUrl} from './ConnectionUrl';

export interface EnvironmentBuilder {
  id?: number,
  name: string,
  connectionUrl: ConnectionUrl,
  disabled_endpoints?: string[],
  theme?: string,
}

