import {Injectable} from '@angular/core';
import {EnvironmentsService} from './environments.service';
import {HttpResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {RequestResponse} from '../../query_builder/models/RequestResponse';

export interface IndexDefinition {
  index: string;
  pri_store_size: string;
  health: string;
  status: string;
  pri: number;
  rep: number;
  docs_count: number;
  docs_deleted: number;
  store_size: string;
}

export interface AliasDefinition {
  alias: string;
  index: string;
  filter: string;
  routing_index: string;
  routing_search: string;
}

@Injectable()
export class EnvironmentInfoService {
  indexDefinitions: IndexDefinition[] = [];

  constructor(private env: EnvironmentsService) {
    // this.getIndexDefinitions();
  }

  static toNumber(v: string): number {
    return !!v ? parseInt(v, 10) : null;
  }

  remapFields<T>(obj: any, mappings: any): T {
    const newObj: T = {} as T;
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const newName = key.replace('.', '_');
        const newValue = newName in mappings ? mappings[newName](obj[key]) : obj[key];
        newObj[newName] = newValue;
      }
    }
    return newObj;
  }

  getIndexDefinitions() {
    return this.env.sendRequest('GET', '_cat/indices?format=json&pretty', null)
      .pipe(map((e: RequestResponse) => {
        if (!!e.es_body) {
          return e.es_body.map(d => this.remapFields<IndexDefinition>(d, {
            pri: EnvironmentInfoService.toNumber,
            rep: EnvironmentInfoService.toNumber,
            docs_count: EnvironmentInfoService.toNumber,
            docs_deleted: EnvironmentInfoService.toNumber,
          }));
        } else {
          return [];
        }
      }));
  }

  getAliasDefinitions() {
    return this.env.sendRequest('GET', '_cat/aliases?format=json&pretty', null)
      .pipe(map((e: RequestResponse) => {
        if (!!e.es_body) {
          return e.es_body.map(d => this.remapFields<AliasDefinition>(d, {}));
        } else {
          return [];
        }
      }));
  }

  getTypesForIndices(indices: string = '_all') {


    return this.env.sendRequest('GET', `${indices}/_mapping?include_type_name`, null)
      .pipe(map((e: RequestResponse) => {
        const types = new Map<string, string[]>();
        if (!!e.es_body) {
          Object.keys(e.es_body).forEach(index => {
            Object.keys(e.es_body[index]['mappings']).forEach(type => {
              if(!types.has(type)) {
                types.set(type, []);
              }
              types.get(type).push(index);
            })
          });
        }
        return Array.from(types.entries()).map(e => { return {
          type: e[0],
          indices: e[1]
        }})
      }));
  }
}
