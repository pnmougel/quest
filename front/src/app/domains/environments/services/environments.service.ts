import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {RequestResponse} from '../../query_builder/models/RequestResponse';
import {flatMap, map} from 'rxjs/operators';
import {ConnectionUrl} from '../models/ConnectionUrl';
import {EnvironmentBuilder} from '../models/EnvironmentBuilder';
import {Environment} from '../models/Environment';

@Injectable()
export class EnvironmentsService {
  private _currentEnvironment: Environment;
  get currentEnvironment(): Observable<Environment> {
    if(!!this._currentEnvironment) {
      return of(this._currentEnvironment);
    } else {
      const environmentId = localStorage.getItem('environmentId');
      if(!!environmentId) {
        return this.getEnvironment(environmentId).pipe(map(environment => {
          // @ts-ignore
          const env = Object.assign(new Environment(), environment) as Environment;
          this._currentEnvironment = env;
          return env;
        }));
      }
    }
  }

  public readonly environmentChanged: Subject<Environment>;

  constructor(private http: HttpClient) {
    this.environmentChanged = new Subject<Environment>();
  }

  getEnvironment(environmentId: string): Observable<Environment> {
      return this.http.get<Environment>(`/environments/${environmentId}`)
  }

  setCurrentEnvironment(environment: Environment) {
    if(this._currentEnvironment !== environment) {
      this._currentEnvironment = environment;
      localStorage.setItem('environmentId', `${environment.id}`);
      this.environmentChanged.next(environment);
    }
  }

  sendRequest(method: string, path: string, body: any): Observable<RequestResponse> {
    console.log('Sending es request');
    console.log(`Method: ${method}`);
    console.log(`Path: ${path}`);
    return this.usingEnvironment(env => this.http.post<RequestResponse>('/requests', {
      method: method.toUpperCase(),
      path,
      body,
      api: 'fooapi',
      environment: env.id
    }));
  }

  usingEnvironment<T>(f: (environment: Environment) => Observable<T> ) {
    return this.currentEnvironment.pipe(flatMap(env => f(env)))
  }

  deleteEnvironment(environmentId: string) {
    return this.http.delete(`/environments/${environmentId}`)
  }

  createEnvironment(environment: EnvironmentBuilder): Observable<any> {
    return this.http.post<any>(`/environments`, {
      name: environment.connectionUrl.host,
      host: environment.connectionUrl.host,
      port: environment.connectionUrl.port,
      protocol: environment.connectionUrl.protocol,
      disabled_endpoints: [],
      theme: 'local'
    });
  }

  updateEnvironment(environmentId: string, environment: EnvironmentBuilder) {
    return this.http.put(`/environments/${environmentId}`, {
      name: environment.connectionUrl.host,
      host: environment.connectionUrl.host,
      port: environment.connectionUrl.port,
      protocol: environment.connectionUrl.protocol,
      disabled_endpoints: [],
      theme: 'local'
    })
  }

  isConnectionUrlAlive(connectionUrl: ConnectionUrl): Observable<object> {
    return this.http.get(`/environments/alive`, {
      params: {
        protocol: connectionUrl.protocol,
        host: connectionUrl.host,
        port: '' + connectionUrl.port
      }
    })
  }

  listEnvironments(): Observable<Environment[]> {
    return this.http.get<Environment[]>('/environments/search').pipe(map(environments => {
      return environments.map(environment => {
        // @ts-ignore
        return Object.assign(new Environment(), environment) as Environment;
      })
    }))
  }
}

