import {Component, OnInit} from '@angular/core';
import {EnvironmentsService} from '../../services/environments.service';
import {Observable} from 'rxjs';
import {Environment} from '../../models/Environment';

@Component({
  selector: 'environment-select',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class EnvironmentSelect implements OnInit {
  environments$: Observable<Environment[]>;
  currentEnvironmentLabel = 'Select Environment';


  constructor(private environmentService: EnvironmentsService) {
    this.environments$ = environmentService.listEnvironments();
  }

  selectEnvironment(environment: Environment) {
    this.environmentService.setCurrentEnvironment(environment);
    this.currentEnvironmentLabel = `${environment.protocol}://${environment.host}:${environment.port}`;
  }

  ngOnInit() {
    const envObservable = this.environmentService.currentEnvironment;
    if(!!envObservable) {
      envObservable.subscribe(environment => {
        console.log(environment.url);

        this.currentEnvironmentLabel = `${environment.protocol}://${environment.host}:${environment.port}`;
      })
    }
    // console.log('Init select');
    // if(!!this.environmentService.currentEnvironment) {
    //   console.log(this.environmentService.currentEnvironment.url);
    //   this.currentEnvironmentLabel = this.environmentService.currentEnvironment.url;
    // }
  }
}
