import {Component, OnInit} from '@angular/core';
import {EnvironmentsService} from '../../services/environments.service';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {Environment} from '../../models/Environment';

@Component({
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class Environments implements OnInit {
  environments$: Observable<Environment[]>;

  constructor(private router: Router, private environmentsService: EnvironmentsService, private toastr: ToastrService) {

  }

  loadEnvironments() {
    this.environments$ = this.environmentsService.listEnvironments();
    this.environments$.subscribe(environments => {
      console.log(environments);
      if(environments.length === 0) {
        this.router.navigate(['environments/add']);
      }
    });
  }


  ngOnInit() {
    this.loadEnvironments();
  }

  deleteEnvironment(environmentId: number) {

    this.environmentsService.deleteEnvironment('' + environmentId).subscribe(e => {
      this.toastr.success('Environment removed');
      this.loadEnvironments();
    }, error => {
      this.toastr.error(error, 'Error removing environment');
    });
  }
  // addEnvironment()
}
