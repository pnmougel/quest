import { Injectable } from '@angular/core';
import {API_DEFINITION} from '../../autocomplete/autocomplete_js/definitions/api_definition';
import {ApiDefinition} from '../models/ApiDefinition';
import {Endpoint} from '../models/Endpoint';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {
  apiDefinitions: ApiDefinition[] = [];
  endpoints: Endpoint[] = [];
  endpointUsages = new Map<string, number>();

  incrementEndpointUsage(endpoint: Endpoint) {
    localStorage.setItem(endpoint.id, ''  + (this.getEndpointUsage(endpoint) + 1))
  }

  getEndpointUsage(endpoint: Endpoint) {
    if(!this.endpointUsages.has(endpoint.id)) {
      let value = localStorage.getItem(endpoint.id);
      if(!value) {
        value = '0';
      }
      this.endpointUsages.set(endpoint.id, parseInt(value));
    }
    return this.endpointUsages.get(endpoint.id);
  }

  getAllEndpoints(): Observable<object[]> {
    return this.http.get<object[]>('/api/endpoints');
  }

  getEndpointTags(): Observable<object[]> {
    return this.http.get<object[]>('/api/endpoint_tags');
  }

  constructor(private http: HttpClient) {
    for (const endpoint in API_DEFINITION.es_6_0.endpoints) {
      const apiDefinition = ApiDefinition.fromJSON(API_DEFINITION.es_6_0.endpoints[endpoint]);
      this.apiDefinitions.push(apiDefinition);
      apiDefinition.patterns.forEach(pathPattern => {
        this.endpoints.push(new Endpoint(apiDefinition, pathPattern));
      });
    }
    this.endpoints.sort((a, b) => this.getEndpointUsage(b) - this.getEndpointUsage(a));
  }

}
