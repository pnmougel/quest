import {ApiDefinition} from './ApiDefinition';

export interface PathElement {
  name: string;
  isParameter: boolean;
  paramPosition: number;
}

export class Endpoint {
  public readonly pathParameters: string[];
  public readonly id;
  public readonly pathElements: PathElement[];

  constructor(public readonly api: ApiDefinition, public readonly pathPattern: string) {
    this.pathParameters = this.pathPattern.match(/({[a-z_]*})/g);
    if(!this.pathParameters) {
      this.pathParameters = [];
    }

    // Build path elements
    let paramPosition = -1;
    this.pathElements = pathPattern.split('/').map(pathElement => {
      const isParameter = pathElement.startsWith('{');
      const name = isParameter ? pathElement.substring(1, pathElement.length - 1) : pathElement;
      if(isParameter) {
        paramPosition += 1;
      }
      return {
        name,
        isParameter,
        paramPosition,
      } as PathElement;
    });
    
    this.pathParameters = this.pathParameters.map(pp => pp.substring(1, pp.length - 1));
    this.id = `${this.api.id}/${this.pathPattern}`;
  }
}
