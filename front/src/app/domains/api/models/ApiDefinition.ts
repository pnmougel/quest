import {JsonToObject} from '../../../core/utils/JsonToObject';

export class ApiDefinition {
  urlParams: object;
  methods: string[];
  patterns: string[];
  documentation: string;
  id: string;
  dataAutocompleteRules: object;

  pathParams: string[][] = [];

  constructor() {
    this.patterns.forEach((pattern, i) => {
      this.pathParams.push(pattern.match(/({[a-z_]*})/g));
    });
  }

  static fromJSON(json: any): ApiDefinition {
    return JsonToObject.fromJSON<ApiDefinition>(json, ApiDefinition.prototype);
  }
}
