import {IEvaluation} from '../models/Evaluation';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';


@Injectable()
export class EvaluationService {
  getEvaluations(): Observable<IEvaluation[]> {

    return of([]);
  }

  deleteEvaluation(evaluationId: string) {
    return of('');
  }
}
