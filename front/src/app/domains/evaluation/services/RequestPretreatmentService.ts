import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {IPretreatment} from '../models/IPretreatment';


@Injectable()
export class RequestPretreatmentService {
  getPretreatments(): Observable<IPretreatment[]> {
    return of([]);
  }

  addIngestPretreatment(name: string, pipeId: string) {

  }

  addScriptPretreatment(name: string, script: string) {

  }

  addEndpointPretreatment(name: string, endpoint: string) {

  }

  deletePretreatment(id: string) {
    return of('');
  }
}
