import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {IDataset} from '../models/IDataset';


@Injectable()
export class DatasetService {
  getDatasets(): Observable<IDataset[]> {
    return of([]);
  }

  addCsvDataset(name: string) {

  }

  addScriptDataset(name: string, script: string) {

  }

  addEndpointDataset(name: string) {

  }

  addManualDataset(name: string, searchTemplateId: string, pretreatmentId: string, indices: string) {

  }

  deleteDataset(id: string) {
    return of('');
  }
}
