import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {EvaluationService} from '../../services/EvaluationService';
import {IEvaluation} from '../../models/Evaluation';

@Component({
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class Evaluations implements OnInit {
  evaluations$: Observable<IEvaluation[]>;

  constructor(private router: Router,
              private evaluationService: EvaluationService,
              private toastr: ToastrService) {
  }

  loadEvaluations() {
    this.evaluations$ = this.evaluationService.getEvaluations();
    this.evaluations$.subscribe(items => {
      if(items.length === 0) {
        this.router.navigate(['evaluation/add']);
      }
    });
  }


  ngOnInit() {
    this.loadEvaluations();
  }

  deleteEvaluation(evaluationId: number) {
    this.evaluationService.deleteEvaluation('' + evaluationId).subscribe(e => {
      this.toastr.success('Evaluation removed');
      this.loadEvaluations();
    }, error => {
      this.toastr.error(error, 'Error removing evaluation');
    });
  }
}
