import {Component, OnInit} from '@angular/core';
import {forkJoin, Observable, zip} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../api/services/api.service';
import {map} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {EnvironmentBuilder} from '../../../environments/models/EnvironmentBuilder';
import {EnvironmentsService} from '../../../environments/services/environments.service';

@Component({
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class AdditEvaluation implements OnInit {
  endpointsTags$: Observable<object[]>;
  endpoints$: Observable<object[]>;
  taggedEndpoints$: Observable<object[]>;
  environmentId: string = null;
  isEdit = true;
  title = 'Register evaluation';

  protocols = ['http', 'https'];
  environment: EnvironmentBuilder = {
    name: '',
    connectionUrl: {
      protocol: 'http',
      host: 'localhost',
      port: 9200,
    },
    disabled_endpoints: [],
    theme: ''
  };

  connectionUrlStatus: any = {
    isAlive: false
  };

  toggleShowEndpoints(endpointTag: any) {
    endpointTag.showEndpoints = !endpointTag.showEndpoints;
  }

  constructor(private router: Router,
              private route: ActivatedRoute,
              private environmentsService: EnvironmentsService,
              private apiService: ApiService,
              private toastr: ToastrService) {
    this.endpoints$ = this.apiService.getAllEndpoints();
    this.endpointsTags$ = this.apiService.getEndpointTags();
    this.taggedEndpoints$ = forkJoin(this.endpointsTags$, this.endpoints$).pipe(map(([endpointTags, endpoints]) => {
      endpointTags.forEach((endpointTag: any) => {
        endpointTag.endpoints = [];
        endpointTag.showEndpoints = false;
        endpoints.forEach((endpoint: any) => {
          if(!!endpoint.tags && endpoint.tags.indexOf(endpointTag.key) !== -1) {
            endpointTag.endpoints.push(endpoint);
          }
        });
      });
      return endpointTags;
    }));
  }

  ngOnInit() {
    this.environmentId = this.route.snapshot.params.id;
    if(!!this.environmentId) {
      this.environmentsService.getEnvironment(this.environmentId).subscribe(environment => {
        // TODO: Handle missing environment
        this.environment.connectionUrl = {
          protocol: environment.protocol,
          host: environment.host,
          port: environment.port,
        };
        this.checkConnectionUrl();
      });
    }

    this.isEdit = !!this.environmentId;
    if(this.isEdit) {
      this.title = 'Update environment'
    }
    this.checkConnectionUrl();
  }

  createEnvironment() {
    this.environmentsService.createEnvironment(this.environment).subscribe(res => {
      this.router.navigate(['environments/list'])
    }, (error) => {
      this.toastr.error(error, 'Error removing environment');
    });
  }

  updateEnvironment() {
    this.environmentsService.updateEnvironment(this.environmentId, this.environment).subscribe(res => {
      this.router.navigate(['environments/list'])
    }, (error) => {
      this.toastr.error(error, 'Error updating environment');
    });
  }

  checkConnectionUrl() {
    this.environmentsService.isConnectionUrlAlive(this.environment.connectionUrl).subscribe( (res: any) => {
      this.connectionUrlStatus = res;
    });
  }
}
