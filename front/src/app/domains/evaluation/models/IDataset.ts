import {DatasetType} from './DatasetType';

export interface IDataset {
  name: string;
  datasetType: DatasetType;
}
