export enum DatasetType {
  // CSV = 'File',
  // JSON = 'File',
  FILE = 'File',

  ENDPOINT = 'Endpoint',
  MANUAL = 'Manual',
  JS_SCRIPT = 'JsScript',
}
