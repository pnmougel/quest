export enum PretreatmentType {
  INGEST,
  ENDPOINT,
  JS_SCRIPT,
}
