export interface RequestResponse {
  es_body?: any;
  es_status: number;
  is_json_body: boolean;
}

