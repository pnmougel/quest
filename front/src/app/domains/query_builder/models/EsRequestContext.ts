import {Endpoint, PathElement} from '../../api/models/Endpoint';


export interface ParamValue {
  name: string;
  value: string;
}

export class EsRequestContext {
  private _endpoint: Endpoint;
  set endpoint(e: Endpoint) {
    this._endpoint = e;
    this.pathParams = e.pathElements.filter(pathElement => pathElement.isParameter).map(pathElement => {
      return {
        name: pathElement.name,
        value: null
      }
    });
    this.body = '';
    this.method = e.api.methods[0];
  }
  get endpoint() {
    return this._endpoint;
  }

  public pathParams: ParamValue[] = [];
  public body = '';
  public method: string;

  isValidRequest(): boolean {
    return true;
  }

  setParamValue(param: PathElement, value: string) {
    this.pathParams[param.paramPosition].value = value;
  }

  get path(): string {
    let path = this.endpoint.pathPattern;
    this.pathParams.forEach(param => {
      if(!!param.value) {
        path = path.replace(`{${param.name}}`, param.value)
      } else {
        console.log(`Missing value for parameter ${param.name}`);
      }
    });
    return path;
  }
}
