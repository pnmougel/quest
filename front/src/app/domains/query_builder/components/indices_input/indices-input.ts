import {Component, OnInit} from '@angular/core';

import {EnvironmentInfoService} from '../../../environments/services/EnvironmentInfoService';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {EnvironmentsService} from '../../../environments/services/environments.service';

@Component({
  selector: 'indices-input',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class IndicesInput implements OnInit {
  index: string[];
  indices$: Observable<any[]>;
  aliases$: Observable<any[]>;
  values$: Observable<any[]>;

  constructor(private envInfo: EnvironmentInfoService, private environments: EnvironmentsService) {
    this.indices$ = envInfo.getIndexDefinitions().pipe(map(v => {
      return v.map(e => {
        return {
          name: e.index,
          health: e.health,
          status: e.status,
          docs_count: e.docs_count,
          type: 'index'
        };
      });
    }));
    this.aliases$ = envInfo.getAliasDefinitions().pipe(map(v => {
      return v.map(e => {
        return {
          name: e.alias,
          index: e.index,
          type: 'alias'
        };
      });
    }));

    this.values$ = forkJoin(this.aliases$, this.indices$).pipe(map(values => {
      const [aliases, indices] = values;
      return aliases.concat(indices);
    }));
  }

  ngOnInit() {}
}
