import {Component, Input, OnInit} from '@angular/core';

import {EnvironmentInfoService} from '../../../environments/services/EnvironmentInfoService';
import {Observable} from 'rxjs';
import {EsRequestContext} from '../../models/EsRequestContext';
import {TextWidth} from '../../../../core/utils/TextWidth';
import {PathElement} from '../../../api/models/Endpoint';
import {
  AutocompleteFieldValue,
  FieldAutocompleteService,
  FieldAutocompletion
} from '../../../autocomplete/services/fieldAutocomplete.service';
import {EnvironmentsService} from '../../../environments/services/environments.service';

@Component({
  selector: 'path-param-input',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class PathParamInput implements OnInit {
  @Input() requestCtx: EsRequestContext;
  @Input() pathParam: PathElement;

  autocompleter: FieldAutocompletion;

  index: string[];
  values$: Observable<AutocompleteFieldValue[]>;

  inputWidth = 100;

  handleFocus() {
    if (!!this.index) {
      this.inputWidth = Math.max(300, this.getTextWidth());
    } else {
      this.inputWidth = 300;
    }
  }

  addTagHandler(term: string): AutocompleteFieldValue {
    return {
      value: term
    };
  }

  handleBlur() {
    this.inputWidth = Math.max(100, this.getTextWidth());
  }

  pathParamChanged(value) {
    // @ts-ignore
    const newValue = value.map(e => e.value).join(',');
    this.inputWidth = Math.max(300, this.getTextWidth());
    this.requestCtx.setParamValue(this.pathParam, newValue);
  }

  getTextWidth() {
    if (!!this.index) {
      // @ts-ignore
      const text = this.index.map(e => e.value).join(',');
      return TextWidth.getTextWidth(text, 14) + 50;
    } else {
      return 0;
    }
  }

  constructor(private envInfo: EnvironmentInfoService,
              private fieldAutocompleteService: FieldAutocompleteService,
              private environments: EnvironmentsService) {
    this.environments.environmentChanged.subscribe(env => {
      // console.log('Environment updated detected');
      this.values$ = this.autocompleter.getAutocompleteValues(this.requestCtx, this.envInfo);
    });
  }

  ngOnInit() {
    this.autocompleter = this.fieldAutocompleteService.getAutocompleter(this.pathParam.name);
    this.values$ = this.autocompleter.getAutocompleteValues(this.requestCtx, this.envInfo);
  }
}
