import {Component, Input, OnInit} from '@angular/core';

import * as ace from 'brace';
import 'brace/theme/monokai';
import 'brace/mode/json';

import {Editor} from 'brace';

@Component({
  selector: 'es-response',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class EsResponse implements OnInit {
  constructor() { }

  @Input() set data(q: string) {
    if (!!this.editor && !!q) {
      this.editor.setValue(JSON.stringify(q, null, 2));
    }
  }
  editor: Editor;

  ngOnInit() {
    this.editor = ace.edit('json-editor');
    this.editor.$blockScrolling = Infinity;

    this.editor.setTheme('ace/theme/monokai');
    this.editor.getSession().setMode('ace/mode/json');
    this.editor.setOptions({
      readOnly: true,
      enableBasicAutocompletion: true,
      printMargin: false,
      fixedWidthGutter: true,
    });

    // this.editor.setValue(JSON.stringify(this.data, null, 2));
  }
}
