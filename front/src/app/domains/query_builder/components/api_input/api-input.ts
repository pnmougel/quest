import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EnvironmentInfoService} from '../../../environments/services/EnvironmentInfoService';
import {ApiDefinition} from '../../../api/models/ApiDefinition';
import {ApiService} from '../../../api/services/api.service';
import {EsRequestContext} from '../../models/EsRequestContext';
import {Endpoint} from '../../../api/models/Endpoint';

@Component({
  selector: 'api-input',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class ApiInput implements OnInit {
  endpoints: Endpoint[] = [];

  @Input() requestCtx: EsRequestContext;
  @Output() endpointChanged = new EventEmitter<Endpoint>();

  handleApiChanged($event) {
    this.endpointChanged.emit($event);
  }

  search(term: string, item: Endpoint): boolean {
    return item.api.id.split('.').some(i => i.startsWith(term));
  }

  constructor(private envInfo: EnvironmentInfoService, private apiService: ApiService) {
    this.endpoints = apiService.endpoints;
  }

  ngOnInit() {
  }
}
