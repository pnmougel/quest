import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import InputMode from '../../../autocomplete/autocomplete_js/editor_mode/input';
import RowParser from '../../../autocomplete/autocomplete_js/core/row_parser';
import {RequestAutocomplete} from '../../../autocomplete/services/autocomplete.service';

import * as ace from 'brace';
import 'brace/theme/chrome';
import {EnvironmentsService} from '../../../environments/services/environments.service';
import {EsRequestContext} from '../../models/EsRequestContext';

@Component({
  selector: 'request-editor',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class RequestEditor implements OnInit {
  @Input() query: EsRequestContext;
  // @Output() queryChange = new EventEmitter<string>();

  @Output() sendQueryEvent = new EventEmitter();

  editor: AceRequestEditor;

  constructor(private autoCompleteService: RequestAutocomplete, private environmnentService: EnvironmentsService) { }

  ngOnInit() {
    this.editor = ace.edit('javascript-editor');
    this.editor.$blockScrolling = Infinity;

    const langTools = ace.acequire('ace/ext/language_tools');
    this.editor.getSession().setMode(new InputMode());
    this.editor.setTheme('ace/theme/chrome');
    this.editor.setOptions({
      enableBasicAutocompletion: true,
      printMargin: false,
      fixedWidthGutter: true,
    });
    this.editor.parser = new RowParser(this.editor);
    this.editor.iterForPosition = (row, column) => {
      return new (ace.acequire('ace/token_iterator').TokenIterator)(this.editor.getSession(), row, column);
    };
    this.editor.iterForCurrentLoc = () => {
      const pos = this.editor.getCursorPosition();
      return this.editor.iterForPosition(pos.row, pos.column, this.editor);
    };
    langTools.setCompleters([{
      identifierRegexps: [/[a-zA-Z_0-9\.\$\-\u00A2-\uFFFF]/], // adds support for dot character
      getCompletions: this.autoCompleteService.getCompletions(this.editor)
    }]);

    this.editor.setValue(this.query.body);

    this.editor.addEventListener('change', (evt) => {
      this.query.body = this.editor.getValue();
      // this.queryChange.emit(this.editor.getValue());
    });

    this.editor.commands.addCommand({
      name: 'showKeyboardShortcuts',
      bindKey: {win: 'Ctrl-Enter', mac: 'Command-Enter'},
      exec: () => {
        this.sendQueryEvent.emit();
      }
    });
  }
}

interface AceRequestEditor extends ace.Editor {
  parser?: RowParser;
  iterForPosition?: (a, b, c) => ace.TokenIterator;
  iterForCurrentLoc?: () => ace.TokenIterator;
}
