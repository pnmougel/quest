import {Component, OnInit} from '@angular/core';
import {EnvironmentsService} from '../../environments/services/environments.service';
import {EnvironmentInfoService} from '../../environments/services/EnvironmentInfoService';
import {EsRequestContext} from '../models/EsRequestContext';
import {ApiService} from '../../api/services/api.service';
import {Endpoint} from '../../api/models/Endpoint';
import {RequestResponse} from '../models/RequestResponse';

@Component({
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class QueryBuilder implements OnInit {
  jsonResponse = {};
  requestCtx: EsRequestContext = new EsRequestContext();

  constructor(private environmentService: EnvironmentsService,
              private envInfo: EnvironmentInfoService,
              private apiService: ApiService) {
    this.requestCtx.endpoint = apiService.endpoints[0];
    this.requestCtx.method = this.requestCtx.endpoint.api.methods[0];

    environmentService.listEnvironments().subscribe(res => {
      console.log(res);
    })
  }

  handleApiChanged(endpoint: Endpoint) {
    this.requestCtx.method = endpoint.api.methods[0];
    this.apiService.incrementEndpointUsage(endpoint);
  }

  sendRequest() {
    this.environmentService.sendRequest(this.requestCtx.method, this.requestCtx.path, this.requestCtx.body).subscribe((e: RequestResponse)  => {
      this.jsonResponse = e.es_body;
    });
  }

  ngOnInit() {

  }
}
