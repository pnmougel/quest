import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QueryBuilder} from '../../domains/query_builder/views/query-builder';
import {Environments} from '../../domains/environments/views/search/Environments';
import {AdditEnvironment} from '../../domains/environments/views/addit/AdditEnvironment';
import {Evaluations} from '../../domains/evaluation/views/search/Evaluations';
import {AdditEvaluation} from '../../domains/evaluation/views/addit/AdditEvaluation';

const routes: Routes = [
  {path: 'query-builder', component: QueryBuilder},

  // Evaluation
  {path: 'evaluation/list', component: Evaluations},
  {path: 'evaluation/edit/:id', component: AdditEvaluation},
  {path: 'evaluation/add', component: AdditEvaluation},

  // Environment
  {path: 'environments/list', component: Environments},
  {path: 'environments/edit/:id', component: AdditEnvironment},
  {path: 'environments/add', component: AdditEnvironment},
  {
    path: '',
    redirectTo: '/environments',
    pathMatch: 'full'
  },
  {path: '**', component: QueryBuilder}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

