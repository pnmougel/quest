
export class JsonToObject {

  static keysToCamel(o) {
    const isObject = (obj) => obj === Object(o) && !Array.isArray(obj) && typeof obj !== 'function';
    const toCamel = (fieldName) => fieldName.replace(/([-_][a-z])/ig, ($1) => $1.toUpperCase().replace('-', '').replace('_', ''));
    if (isObject(o)) {
      const n = {};
      Object.keys(o)
        .forEach((k) => {
          n[toCamel(k)] = JsonToObject.keysToCamel(o[k]);
        });

      return n;
    } else if (Array.isArray(o)) {
      return o.map((i) => {
        return JsonToObject.keysToCamel(i);
      });
    }
    return o;
  }


  static fromJSON<T>(json: any, proto: any): T {
    let obj: T = Object.create(proto) as T;
    obj = Object.assign(obj, JsonToObject.keysToCamel(json)) as T;
    obj.constructor();
    // obj.init();
    return obj;
  }
}
