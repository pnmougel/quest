export class TextWidth {
  static canvas: HTMLCanvasElement;
  static context: CanvasRenderingContext2D;

  static getTextWidth(text: string, fontSize: number, fontWeight: string = 'normal', fontFamily: string = 'Roboto'): number {
    if(!TextWidth.canvas) {
      TextWidth.canvas = document.createElement('canvas') as HTMLCanvasElement;
      TextWidth.context = TextWidth.canvas.getContext('2d')
    }
    TextWidth.context.font = `${fontWeight} ${fontSize}px ${fontFamily}`;
    return TextWidth.context.measureText(text).width;
  }
}
