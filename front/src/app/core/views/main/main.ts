import {Component, OnInit} from '@angular/core';
import {EnvironmentsService} from '../../../domains/environments/services/environments.service';

@Component({
  selector: 'app',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class App implements OnInit {
  public constructor(private environmentService: EnvironmentsService) {
  }

  menuElements = [{
    icon: 'icon-search-1',
    label: 'Request builder',
    url: '/query-builder'
  }, {
    icon: 'icon-folder-binders',
    icon_svg: '23-content/book-shelf.svg',
    label: 'Indices'
  }, {
    icon: 'icon-business-chart-3',
    label: 'Evaluation',
    url: '/evaluation/list'
  }, {
    icon: 'icon-loading-3',
    label: 'Environments',
    url: '/environments/list'
  }];

  ngOnInit() {

  }
}
